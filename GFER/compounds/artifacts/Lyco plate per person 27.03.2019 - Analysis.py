#!/usr/bin/env python
# coding: utf-8

# # Lyco plate per person 27.03.2019 - Analysis
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # Alon's Data

# ## Loading the data

# In[6]:


data_path = "../Data/Lyco/Lyco plate per person 27.03.2019/Alon 24h - agg_results.csv"
df = pd.read_csv(data_path, index_col=0)
df = df.drop(labels='index', axis=1)
df = df[df.nuc_outlier==False]
df = df[df.cyto_outlier==False]
df = df[df.Cyto_border_case==False]
df = df.drop(labels=['nuc_outlier', 'cyto_outlier', 'Cyto_border_case'], axis=1)
df.columns = df.columns.str.replace("GFER", "Cox17") # This is due to a naming mistake at the data creation


# In[7]:


df.sample(7)


# In[8]:


# Adding Compunds and concentration 
df["Compound"] = np.nan
df.loc[df.column.isin([2, 3]), "Compound"] = "Control"
df.loc[df.column.isin([4, 5]), "Compound"] = "DMSO"
df.loc[df.column.isin([6, 7]), "Compound"] = "Lyco Low"
df.loc[df.column.isin([8, 9]), "Compound"] = "Lyco Medium"
df.loc[df.column.isin([10, 11]), "Compound"] = "Lyco High"
display('ok'if df["Compound"].isna().any()==False else ':-(')


# In[9]:


compound_plate = df.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# ## Number of cells 

# In[10]:


cell_in_well = df.groupby(["row", "column"])["Compound"].count().unstack(level=-1)
ax = sns.heatmap(cell_in_well, linewidths=0.5, annot=True, fmt="0.00f")
ax.xaxis.set_ticks_position('top')


# In[11]:


order = ['Control', 'DMSO', 
         'Lyco Low', 'Lyco Medium', 'Lyco High'
        ]


# In[12]:


g = df.groupby(['Compound'])["row"].count().reset_index()
display(g)
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="row", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Cells Count")
ax.legend(loc = 1)
plt.tight_layout()


# ## Analyzing the features

# In[13]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cyto_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title("Cyto_size")
plt.tight_layout()


# In[14]:


g = df.groupby([ 'Compound'])["Cyto_size"].sum().reset_index()

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="Cyto_size", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Cyto size sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[15]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_avg", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity avg')
plt.tight_layout()


# In[16]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity sum')
plt.tight_layout()


# In[17]:


df.columns.tolist()


# In[18]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[19]:


df['Cox17_size*'] = df['Cox17_size']/df['Cyto_size']

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[20]:


df['Cox17_size**'] = df['Cox17_size']/df['MitoTracker_size_sum']

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size**", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[21]:


# Cox17 IxA
df['Cox17_sum'] = df['Cox17_avg']*df['Cox17_size']
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[22]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_count", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[23]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_mean", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[24]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[25]:


df['MitoTracker_size_sum*'] = df['MitoTracker_size_sum']/df['Cyto_size']
sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_sum*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[26]:


g=df.groupby(['Compound'], as_index=False)["MitoTracker_size_sum"].sum()


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=False)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="MitoTracker_size_sum", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Sum of MitoTracker_size_sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[27]:


g=df.groupby(['Compound'], as_index=False)[["Cox17_in_MitoTracker_size", "Cox17_size"]].sum()
g["Cox17_in_MitoTracker_size_ratio"] = g["Cox17_in_MitoTracker_size"]/g["Cox17_size"]


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=False)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="Cox17_in_MitoTracker_size_ratio", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Sum of MitoTracker_size_sum")
ax.legend(loc = 1)
plt.tight_layout()


# # Ella's Data

# ## Loading the data

# In[28]:


data_path = "../Data/Lyco/Lyco plate per person 27.03.2019/Ella 24h - agg_results.csv"
df = pd.read_csv(data_path, index_col=0)
df = df.drop(labels='index', axis=1)
df = df[df.nuc_outlier==False]
df = df[df.cyto_outlier==False]
df = df[df.Cyto_border_case==False]
df = df.drop(labels=['nuc_outlier', 'cyto_outlier', 'Cyto_border_case'], axis=1)


# In[29]:


df.sample(7)


# In[30]:


# Adding Compunds and concentration 
df["Compound"] = np.nan
df.loc[df.column.isin([2, 3]), "Compound"] = "Control"
df.loc[df.column.isin([4, 5]), "Compound"] = "DMSO"
df.loc[df.column.isin([6, 7]), "Compound"] = "Lyco Low"
df.loc[df.column.isin([8, 9]), "Compound"] = "Lyco Medium"
df.loc[df.column.isin([10, 11]), "Compound"] = "Lyco High"
display('ok'if df["Compound"].isna().any()==False else ':-(')


# In[31]:


compound_plate = df.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# ## Number of cells
# 

# In[32]:


cell_in_well = df.groupby(["row", "column"])["Compound"].count().unstack(level=-1)
ax = sns.heatmap(cell_in_well, linewidths=0.5, annot=True, fmt="0.00f")
ax.xaxis.set_ticks_position('top')


# In[33]:


order = ['Control', 'DMSO', 
         'Lyco Low', 'Lyco Medium', 'Lyco High'
        ]


# In[34]:


g = df.groupby(['Compound'])["row"].count().reset_index()
display(g)
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="row", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Cells Count")
ax.legend(loc = 1)
plt.tight_layout()


# ## Analyzing the features

# In[35]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cyto_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title("Cyto_size")
plt.tight_layout()


# In[36]:


g = df.groupby([ 'Compound'])["Cyto_size"].sum().reset_index()

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="Cyto_size", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Cyto size sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[38]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_avg", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity avg')
plt.tight_layout()


# In[39]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity sum')
plt.tight_layout()


# In[40]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[41]:


df['Cox17_size*'] = df['Cox17_size']/df['Cyto_size']

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[42]:


df['Cox17_size**'] = df['Cox17_size']/df['MitoTracker_size_sum']

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size**", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[43]:



# Cox17 IxA
df['Cox17_sum'] = df['Cox17_avg']*df['Cox17_size']
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
ax.set_title('Cox17 intesity size')
plt.tight_layout()


# In[44]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_count", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[46]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_mean", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[47]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_sum", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[48]:


df['MitoTracker_size_sum*'] = df['MitoTracker_size_sum']/df['Cyto_size']
sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="Compound", y="MitoTracker_size_sum*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order, order=order)
plt.tight_layout()


# In[49]:



g=df.groupby(['Compound'], as_index=False)["MitoTracker_size_sum"].sum()


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=False)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="MitoTracker_size_sum", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Sum of MitoTracker_size_sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[50]:


g=df.groupby(['Compound'], as_index=False)[["Cox17_in_MitoTracker_size", "Cox17_size"]].sum()
g["Cox17_in_MitoTracker_size_ratio"] = g["Cox17_in_MitoTracker_size"]/g["Cox17_size"]


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=False)
fig.set_size_inches(19, 8)
sns.barplot(x="Compound", y="Cox17_in_MitoTracker_size_ratio", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order, order=order);
ax.set_title("Sum of MitoTracker_size_sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[ ]:




