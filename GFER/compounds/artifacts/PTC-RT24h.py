
# coding: utf-8

# # PTC Readthrough 24h
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[3]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[27]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[28]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[29]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()


# In[30]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # Generating the data

# In[31]:


# Define the protocol

channel_map = {"DAPI - DAPI": "NucliChannel",
               "Cy3 - Cy3": ["SemiCytoChannel", "ActinChannel"],
               "Cy5 - Cy5": "MitoTrackerChannel",
               "FITC - FITC": "GferChannel",
               "DataExtractors": [{"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                       "trigger_after": "Nucli"},
                      {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                       "trigger_after": "Nucli"},
                      {"class": "SemiCytoImageUnification", "data_sources": ["Cyto", "Nucli", "GFER", "MitoTracker"],
                       "trigger_after": "Nucli"},
                      {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                       "trigger_after": "Cyto"},
                      {"class": "MitoTrackerDataExtractor", "data_sources": ["MitoTracker"],
                       "trigger_after": "MitoTracker"},                                 
                      {"class": "AntiBodyDataExtractor", "data_sources": ["GFER"],
                       "trigger_after": "GFER"},
                      {"class": "AntiBodyDataExtractor", "data_sources": ["Actin"],
                       "trigger_after": "Actin"},
                      {"class": "ContoursExporter", "data_sources": ["Cyto"],
                       "trigger_after": "Cyto"}]
               }


# In[32]:


# Running folder loader
import os, sys
segmenter_path = "C:\\Code\\CellDoctor"
sys.path.append(segmenter_path) 

from Segmenter.FolderLoader import FolderLoader 


images_path = "G:\\BioData\\Experiments\\Images\\lyco and comp\\Standard lab acquisition protocol GFER IF\\Standard lab acquisition protocol GFER IF_comp48h_mitogferandcox280119_1"
# FolderLoader(images_path, protocol_channel_map=channel_map, parallel_processing=False, continue_existing_session=True)


# In[33]:


from Segmenter.DataProcessor.ImageDataUnifier import ImageDataUnifier

results_path = images_path+'\\results'


unification_protocol = [("base", {"ch_type": "Nucli"}),
                         ("join", {"ch_type": "Cyto", "full_tbl_fld": "cyto_label"}),
                         ("join", {"ch_type": "GFER", "full_tbl_fld": "cyto_label"}),
                         ("join", {"ch_type": "Actin", "full_tbl_fld": "cyto_label"}),
                         ("join_agg", {"ch_type": "MitoTracker", "full_tbl_fld": "cyto_label"}),
                        ]

# ImageDataUnifier(folder_path=results_path,unification_protocol=unification_protocol)


# # General

# In[34]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 36.5*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": 53*12*4, "gender": "M", "color": "coral"},
            "HD":{"age": np.nan, "gender": np.nan, "color": "orange"}}
patients_df = pd.DataFrame(patients).T
patients_df


# # Loading the data

# ## General

# In[35]:


data_path = "../Data/RT/24h/agg_results.csv"
df = pd.read_csv(data_path, index_col=0)
df = df.drop(labels='index', axis=1)
df = df[df.nuc_outlier==False]
df = df[df.cyto_outlier==False]
df = df[df.Cyto_border_case==False]
df = df.drop(labels=['nuc_outlier', 'cyto_outlier', 'Cyto_border_case'], axis=1)


# In[36]:


df.sample(7)


# ## Transforming the experiment specifics
# 

# <img src="../Data/RT/24h/IMG_20190212_152730.jpg"  style="width: 800px; height: 400px"/>

# In[37]:


# Replacing the name GFER with AntiBody as the plate contain also Cox17
df.columns = df.columns.str.replace("GFER", "AntiBody")


# In[38]:


# Adding patients name
col_map = {2:"AV", 3:"AV", 4:"AV", 5:"AV", 6:"EV", 7:"EV", 8:"EV", 9:"EV", 10:"HD", 11:"AG015"}
df["patient"] = df.column.map(col_map)


# In[39]:


# Adding Antibodies names
df["AntiBodyType"] = np.nan
df.loc[df.column.isin([2,4,6,8]), "AntiBodyType"] = "GFER"
df.loc[df.column.isin([3,5,7,9]), "AntiBodyType"] = "Cox17"
df.loc[df.column.isin([10,11]) & df.row.isin(['B', 'D', 'F']), "AntiBodyType"] = "GFER"
df.loc[df.column.isin([10,11]) & df.row.isin(['C', 'E', 'G']), "AntiBodyType"] = "Cox17"
df["AntiBodyType"].isna().any()


# In[40]:


antibody_plate = df.groupby(['column', 'row']).AntiBodyType.first().unstack(level=-1).T
antibody_plate 


# In[41]:


# Adding Compunds and concentration 
df["Compound"] = np.nan
df.loc[(df.row=='B') & (df.column.isin([2,3,6,7])), "Compound"] = "Control"
df.loc[(df.row=='B') & (df.column.isin([4,5,8,9])), "Compound"] = "DMSO"

df.loc[(df.row=='C') & (df.column.isin([2,3,6,7])), "Compound"] = "PTC124_10"
df.loc[(df.row=='C') & (df.column.isin([4,5,8,9])), "Compound"] = "PTC124_20"

df.loc[(df.row=='D') & (df.column.isin([2,3,6,7])), "Compound"] = "G418_2"
df.loc[(df.row=='D') & (df.column.isin([4,5,8,9])), "Compound"] = "G418_5"

df.loc[(df.row=='E') & (df.column.isin([2,3,6,7])), "Compound"] = "G418_8"
df.loc[(df.row=='E') & (df.column.isin([4,5,8,9])), "Compound"] = "Azithromycin_100"

df.loc[(df.row=='F') & (df.column.isin([2,3,6,7])), "Compound"] = "Azithromycin_350"
df.loc[(df.row=='F') & (df.column.isin([4,5,8,9])), "Compound"] = "Azithromycin_700"

df.loc[(df.row=='G') & (df.column.isin([2,3,6,7])), "Compound"] = "Amlexanox_25"
df.loc[(df.row=='G') & (df.column.isin([4,5,8,9])), "Compound"] = "Amlexanox_50"

df.loc[(df.row.isin(['B','C'])) & (df.column.isin([10,11])), "Compound"] = "Control"
df.loc[(df.row.isin(['D', 'E'])) & (df.column.isin([10,11])), "Compound"] = "PTC124_20"
df.loc[(df.row.isin(['F', 'G'])) & (df.column.isin([10,11])), "Compound"] = "Amlexanox_50"
df["Compound"].isna().any()


# In[42]:


compound_plate = df.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# In[43]:


# (F, 9) have no cells 
df.groupby(['column', 'row'])['Actin_avg'].count()[9]


# # Number of cells 

# In[44]:


cell_in_well = df.groupby(["row", "column"])["AntiBodyType"].count().unstack(level=-1)
ax = sns.heatmap(cell_in_well, linewidths=0.5, annot=True, fmt="0.00f")
ax.xaxis.set_ticks_position('top')


# In[24]:


order = ['Control', 'DMSO', 
         'PTC124_10', 'PTC124_20', 
         'Amlexanox_25', 'Amlexanox_50',
         'G418_2', 'G418_5', 'G418_8', 
         'Azithromycin_100', 'Azithromycin_350', 'Azithromycin_700'
        ]


# In[45]:


g = df.groupby(["row", "column", "patient", "Compound"])["AntiBodyType"].count()
g = g.groupby(["patient", "Compound"]).mean().reset_index()

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="patient", y="AntiBodyType", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order);
ax.set_title("Cells Count")
ax.legend(loc = 1)
plt.tight_layout()


# In[23]:


# TODO: Consider adding a tag of positive effect (greater then 15% increase from DMSO), negative (... -15% ...) and natural (-15%..+15%)


# # Analyzing the features

# In[26]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="patient", y="Cyto_size", hue="Compound", data=df[df.Compound != 'Azithromycin_700'], palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("Cyto_size")
plt.tight_layout()


# As there are cases when the size and count have negative correlation let's integrate it   
# TODO:   
# * Look at the images and see why Azithromycin_700 create huge cells (expected to make theme smaller)

# In[46]:


g = df.groupby(['patient', 'Compound'])["Cyto_size"].sum().reset_index()

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="patient", y="Cyto_size", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order);
ax.set_title("Cyto size sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[116]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_q90", hue="Compound",  data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title("Intesity q90 "+abType)
plt.tight_layout()


# In[119]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_size", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title(f"{abType} size")
plt.tight_layout()


# In[59]:


g=df.groupby(['AntiBodyType', 'patient' , 'Compound'], as_index=False)["AntiBody_size"].median()


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=True)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.barplot(x="patient", y="AntiBody_size", hue="Compound", data=g[(g.Compound != 'Azithromycin_700') & (g.AntiBodyType==abType)], palette="Set3" ,ax=ax[i], ci=None, hue_order=order);
    ax[i].set_title(abType)
    ax[i].legend(loc = 1)
plt.tight_layout()


# In[97]:


g=df.groupby(['AntiBodyType', 'patient' , 'Compound'], as_index=False)["AntiBody_size"].sum()


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=True)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.barplot(x="patient", y="AntiBody_size", hue="Compound", data=g[g.AntiBodyType==abType], palette="Set3" ,ax=ax[i], ci=None, hue_order=order);
    ax[i].set_title(abType)
    ax[i].legend(loc = 1)
plt.tight_layout()


# In[120]:


df['AntiBody_size*'] = df['AntiBody_size']/df['Cyto_size']

sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_size*", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title(abType)
plt.tight_layout()


# Can it be that Alon and Ella require different treatment?
# What can cause it?
# * Different mix of protein allele
# * Different cleaning characteristics

# In[139]:


# TODO: scatter plot with AntiBody_size* and cell count 

# g=df.groupby(['AntiBodyType', 'patient' , 'Compound'], as_index=False)["AntiBody_size*"].agg(['mean', 'count']).reset_index()

# sns.set_style('ticks')
# fig, ax = plt.subplots(1,2, sharey=True)
# fig.set_size_inches(19, 8)
# for i, abType in enumerate(df.AntiBodyType.unique()):
#     sns.scatterplot(x='mean', y='count', hue='Compound', data=g[g.AntiBodyType==abType],palette="Set3" ,ax=ax[i], sizes=50)
#     ax[i].set_title(abType)
#     ax[i].legend(loc = 1)
# plt.tight_layout()


# In[143]:


df['AntiBody_size**'] = df['AntiBody_size']/df['MitoTracker_size_sum']

sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_size**", hue="Compound", data=df[(df.Compound != 'XXX') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title(abType+" size**")
plt.tight_layout()


# In[101]:


# These results does not make sense
# TODO: Look at Cox17 and PTC124_20, DMSO in Ella
# TO Consider re-run with co-localization


# In[102]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=True)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_avg", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title(abType)
plt.tight_layout()


# In[103]:


# AntiBody IxA
df['AntiBody_sum'] = df['AntiBody_avg']*df['AntiBody_size']

sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.boxplot(x="patient", y="AntiBody_sum", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax[i], hue_order=order)
    ax[i].set_title(abType)
plt.tight_layout()


# In[104]:


# TODO: to divid by DMSO


# In[145]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="patient", y="MitoTracker_size_median", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
plt.tight_layout()


# In[146]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="patient", y="MitoTracker_size_sum", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.legend(loc=1)
plt.tight_layout()


# In[147]:


df['MitoTracker_size_sum*'] = df['MitoTracker_size_sum']/df['Cyto_size']

sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="patient", y="MitoTracker_size_sum*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
plt.tight_layout()


# In[57]:


g=df.groupby(['AntiBodyType', 'patient' , 'Compound'], as_index=False)["MitoTracker_size_sum"].median()

sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=True)
fig.set_size_inches(19, 8)
for i, abType in enumerate(df.AntiBodyType.unique()):
    sns.barplot(x="patient", y="MitoTracker_size_sum", hue="Compound", data=g[(g.Compound != 'Azithromycin_700') & (g.AntiBodyType==abType)], palette="Set3" ,ax=ax[i], ci=None, hue_order=order);
    ax[i].set_title("Median MitoTracker_size_sum by "+abType)
    ax[i].legend(loc = 1)
plt.tight_layout()
plt.suptitle("These two should be roughly the same");


# In[108]:


sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(19, 8)
ax = sns.boxplot(x="patient", y="MitoTracker_count", hue="Compound", data=df[(df.Compound != 'Azithromycin_700') & (df.AntiBodyType==abType)], palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.legend(loc=1)
plt.tight_layout()


# In[1]:


# df.columns.tolist()


# * Next round to image after 12h
# * G418 - 1 mg/ml is enough and 2 mg/ml is very high
