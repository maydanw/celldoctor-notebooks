
# coding: utf-8

# # Compounds phase I summary
# ---

# ## General
# 1. We need at least two columns per permutation to get stable meaningful data
# 1. Each experiment should be repeated with location shuffling
# 1. Consider using free fatty acids serum
# 1. Compare few images analysis in the developer vs Cell doctor
# 1. Add dead cell counting
# 
# 
# ## Lycopene
# **Questions:**  
# * Are the increasing dose effect of Lycopene on cell count is mitogenic?
# * The medium is serum free? **serum free**
# * NRF2 proliferation effect? Oncogenic effect?
# * DMSO concentrations equals the Lyco High? **Yes**
# * Is it better seeding double amount of HD in order to have similar confluence? **No the cell size is bigger**
#   
# **To do in this experiment:**
# * Specify the concentrations of serum, DMSO and compounds. **DMSO: 1/1k, Serum free**
# * Look at the images and segmentation
# * Look at the images and understand why when looking at AntiBody_avg the DMSO is better then control?
# * Cox17 seems weird, look at the images (anomalies and look in the photos)
# * Add Cox17 size significance tests - **Seems there is too much noise**
# * Find a way to make significance tests where huge sampling noise is a known parameter
#   
# **Further experiments:**
# * See General notes
# * Bring the cells to confluency and FACS analysis to validate Oncogenic effect
# * Change plate layout and verify proliferation effect
# * In 72h Lycopene it seems like we have an over dose in the high concentration (except maybe HD which react well to it). It may be better to do 24h and 48h (and not 72h)
# 
# ## Read through
# **To do in this experiment:**
# * Look at the images and segmentation especially cell size and AZ 700, 350 (it should be expected they will be smaller)
# * Look at the images and segmentation why GFER is greater then mito? Consider running the experiments with co-localization 
# 
# 
# **Further experiments:**
# 1. Use a plate per child, include Nir in it and add Actin cap into the analysis protocol - **Ask Miguel why do the actin cap**
# 1. Try examine the compounds over 12h 
# 1. Try to look at the plate at 60X - **(no analysis tool)**
# 1. Reduce substances doses and compounds (G418 - if using then only 1mg/ml, AZ - only 50 and 100)
