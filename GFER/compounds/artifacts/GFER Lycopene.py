#!/usr/bin/env python
# coding: utf-8

# # GFER Lycopene
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()


# In[5]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# ## Runinng CellDoctor

# In[6]:


# Define the protocol

channel_map = {"DAPI - DAPI": "NucliChannel",
               "Cy3 - Cy3": ["SemiCytoChannel", "ActinChannel"],
               "Cy5 - Cy5": "MitoTrackerChannel",
               "FITC - FITC": "GferChannel",
               "DataExtractors": [{"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                       "trigger_after": "Nucli"},
                      {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                       "trigger_after": "Nucli"},
                      {"class": "SemiCytoImageUnification", "data_sources": ["Cyto", "Nucli", "GFER", "MitoTracker"],
                       "trigger_after": "Nucli"},
                      {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                       "trigger_after": "Cyto"},
                      {"class": "MitoTrackerDataExtractor", "data_sources": ["MitoTracker"],
                       "trigger_after": "MitoTracker"},                                 
                      {"class": "AntiBodyDataExtractor", "data_sources": ["GFER"],
                       "trigger_after": "GFER"},
                      {"class": "AntiBodyDataExtractor", "data_sources": ["Actin"],
                       "trigger_after": "Actin"},
                      {"class": "ContoursExporter", "data_sources": ["Cyto"],
                       "trigger_after": "Cyto"},                                  
                      {"class": "AntiBodyCoLocalizationDataExtractor", "data_sources": ["GFER", "Nucli"],
                       "trigger_after": "GFER"},
                      {"class": "AntiBodyCoLocalizationDataExtractor", "data_sources": ["GFER", "MitoTracker"],
                       "trigger_after": "GFER"}]
               }


# In[7]:


# Running folder loader
import os, sys
segmenter_path = "F:\\Code\\CellDoctor"
sys.path.append(segmenter_path) 

from Segmenter.FolderLoader import FolderLoader 


images_path = "G:\\BioData\\Experiments\\Images\\lyco and comp\\Standard lab acquisition protocol GFER IF\\Standard lab acquisition protocol GFER IF_lyco72h_mitogferandcox280119_1"
FolderLoader(images_path, protocol_channel_map=channel_map, parallel_processing=False, continue_existing_session=True)


# In[8]:


from Segmenter.DataProcessor.ImageDataUnifier import ImageDataUnifier

results_path = images_path+'\\results'


unification_protocol = [("base", {"ch_type": "Nucli"}),
                         ("join", {"ch_type": "Cyto", "full_tbl_fld": "cyto_label"}),
                         ("join", {"ch_type": "GFER", "full_tbl_fld": "cyto_label"}),
                         ("join", {"ch_type": "Actin", "full_tbl_fld": "cyto_label"}),
                         ("join_agg", {"ch_type": "MitoTracker", "full_tbl_fld": "cyto_label"}),
                        ]

ImageDataUnifier(folder_path=results_path,unification_protocol=unification_protocol)


# In[ ]:





# In[ ]:





# In[ ]:




