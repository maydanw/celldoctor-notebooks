#!/usr/bin/env python
# coding: utf-8

# # GFER Plamitate 12h April 2019
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# # Loading the data

# In[5]:


data_path = "../Data/Palmitate/12h/Standard lab acquisition protocol 5_pal12htmremito160419gfer1_1/agg_results_new.csv"

df = pd.read_csv(data_path, index_col=0)
df = df.drop(labels='index', axis=1)
df = df[df.nuc_outlier==False]
df = df[df.cyto_outlier==False]
df = df[df.Cyto_border_case==False]
df = df.drop(labels=['nuc_outlier', 'cyto_outlier', 'Cyto_border_case'], axis=1)
df.sample(5)


# In[6]:


df["Patient"] = np.nan
df.loc[df.column.isin([2, 3]), "Patient"] = "AV"
df.loc[df.column.isin([4, 5]), "Patient"] = "AG015"
df.loc[df.column.isin([6, 7]), "Patient"] = "EV"
df.loc[df.column.isin([8, 9]), "Patient"] = "CM"
df.loc[df.column.isin([10, 11]), "Patient"] = "progeria127"
display('ok'if df["Patient"].isna().any()==False else ':-(')


# In[7]:


patient_plate = df.groupby(['column', 'row']).Patient.first().unstack(level=-1).T
patient_plate


# _normal_ - normal media with 10% FBS  
# _palmitate_ - palmitate media with 10% BSA and 10% dyalized serum 

# In[8]:


df["Compound"] = np.nan
df.loc[df.column.isin([2,4,6,8,10]), "Compound"] = "Normal"
df.loc[df.column.isin([3,5,7,9,11]), "Compound"] = "Palmitate"

display('ok'if df["Patient"].isna().any()==False else ':-(')


# In[9]:


compound_plate = df.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# # Number of cells 
# 

# In[10]:


cell_in_well = df.groupby(["row", "column"])["Compound"].count().unstack(level=-1)
ax = sns.heatmap(cell_in_well, linewidths=0.5, annot=True, fmt="0.00f")
ax.xaxis.set_ticks_position('top')


# In[11]:


order = ['Normal', 'Palmitate']


# In[12]:


g = df.groupby(["row", "column", "Patient", "Compound"])["Cyto_intensity_avg"].count()
g = g.groupby(["Patient", "Compound"]).mean().reset_index()


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Patient", y="Cyto_intensity_avg", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order);
ax.set_title("Cells Count")
ax.legend(loc = 1)
plt.tight_layout()


# # Analyzing the features

# In[13]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="CELL AREA", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("Cyto_size")
plt.tight_layout()


# Progeria cell area is very distinctly different.  
# In all cases Palmitate decreased the cell area (excluding the Progeria cells).  

# In[16]:


g = df.groupby(['Patient', 'Compound'])["CELL AREA"].sum().reset_index()

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.barplot(x="Patient", y="CELL AREA", hue="Compound", data=g, palette="Set3" ,ax=ax, ci=None, hue_order=order);
ax.set_title("Cyto area sum")
ax.legend(loc = 1)
plt.tight_layout()


# In[17]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE INTENSITY", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE INTENSITY")
plt.tight_layout()


# In[18]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE mean int", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE mean int")
plt.tight_layout()


# In[19]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE AREA", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE AREA")
plt.tight_layout()


# In[20]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE mean area", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE mean area")
plt.tight_layout()


# In[21]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE DxA", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE DxA")
plt.tight_layout()


# In[31]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y='TMRE COUNT', hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title('TMRE COUNT')
plt.tight_layout()


# In[22]:


df['TMRE AREA*'] = df['TMRE AREA']/df['CELL AREA']
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y='TMRE AREA*', hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title('TMRE AREA*')
plt.tight_layout()


# In[23]:


df['TMRE AREA**'] = df['TMRE AREA']/df['MITOTRECKER AREA']

sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="TMRE AREA**", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("TMRE AREA**")
plt.tight_layout()


# As the numbers are above 1.0, does it means there is a lot more TMRE then mitotracker?   
# Lets look at the mitochondria

# In[24]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="MITOTRACKER COUNT", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("MITOTRACKER COUNT")
plt.tight_layout()


# In[25]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="MITOTRECKER AREA", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("MITOTRECKER AREA")
plt.tight_layout()


# In[26]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="MITOTRACKER DxA", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("MITOTRACKER DxA")
plt.tight_layout()


# In[27]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="MITOTRACKER INTENSITY", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("MITOTRACKER INTENSITY")
plt.tight_layout()


# In all aspects it seems like Palmitate is not doing so good to the mitochondria

# In[28]:


df['MITOTRECKER AREA*'] = df['MITOTRECKER AREA']/df['CELL AREA']
sns.set_style('ticks')
fig, ax = plt.subplots(1,1, sharey=True)
fig.set_size_inches(19, 8)
sns.boxplot(x="Patient", y="MITOTRECKER AREA*", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax, hue_order=order)
ax.set_title("MITOTRECKER AREA*")
plt.tight_layout()


# It look like given Palmitate the TMRE area increases while the MitoTracker area decreases

# In[ ]:





# In[32]:


# df.columns.tolist()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




