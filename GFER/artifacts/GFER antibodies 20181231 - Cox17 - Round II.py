#!/usr/bin/env python
# coding: utf-8

# # GFER antibodies 20181231 - Cox17 - Round II
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

# Do run in the first time: jupyter nbextension enable --py --sys-prefix widgetsnbextension


# In[4]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# In[100]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 36.5*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": 53*12*4, "gender": "M", "color": "coral"}}
patients_df = pd.DataFrame(patients).T
patients_df[['age', 'gender', 'color']]


# In[6]:


from scipy.stats import mannwhitneyu

def compare_results(df, base_key, feature_name, df_grpby_key='Pcol', hyp_alternative="less"):
    df_grp_row = df.groupby(df_grpby_key)
    base_data = df_grp_row[feature_name].get_group(base_key)

    p_values = {}
    for grp_k in df_grp_row[feature_name].groups.keys():
        if grp_k==base_key:
            continue
        p_data = df_grp_row[feature_name].get_group(grp_k)
        stat, p = mannwhitneyu(base_data, p_data, alternative=hyp_alternative)
        p_values[grp_k] = p
    return p_values


# In[7]:


def pval_corrected(pvals_raw, method=None):
    '''p-values corrected for multiple testing problem
 
    This uses the default p-value correction of the instance stored in
    ``self.multitest_method`` if method is None.
 
    '''
    import statsmodels.stats.multitest as smt
    if method is None:
        method = 'Bonferroni'
    #TODO: breaks with method=None
    return smt.multipletests(pvals_raw, method=method)[1]


# In[8]:


from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource, CategoricalColorMapper, LabelSet
from bokeh.palettes import Set3
from bokeh.palettes import d3
from bokeh.models.tools import HoverTool
from bokeh.models import NumeralTickFormatter


def plot_column(df, column_name, agg='50%'):

    TOOLS='pan,wheel_zoom,box_zoom,reset'

    source = ColumnDataSource(df)
    patients_names =  source.data['Pcol'].tolist()
    plot_options = dict(width=750, plot_height=350,tools=TOOLS)
    p = figure(**plot_options, x_range=patients_names)


    # palette = d3['Category10'][len(patients_names)]
    # palette = [patients[pn]['color'] for pn in patients_names]
    # color_map = CategoricalColorMapper(factors=patients_names, palette=palette)
    
    source.data['color'] = [patients[pn]['color'] for pn in patients_names]
    source.data['age'] = [patients[pn]['age']/4/12 for pn in patients_names]
    source.data['gender'] = [patients[pn]['gender'] for pn in patients_names]
    
    cell_count = df["Pcol"].count().to_dict()
    source.data['cell_count'] = [cell_count[pn] for pn in patients_names]

    # renderer = p.vbar(x='Pcol', top='Cox17 IxA_50%', width=0.95, line_color="gray", source=source, color={'field': 'Pcol', 'transform': color_map})
    renderer = p.vbar(x='Pcol', top=f"{column_name}_{agg}", width=0.95, line_color="gray", source=source, color='color')
    
    source.data[f"formatted_{column_name}_{agg}"] = [f"{x:,.4f}" if x<10 else f"{x:,.0f}" for x in source.data[f"{column_name}_{agg}"]]
    labels = LabelSet(x='Pcol', y=f"{column_name}_{agg}", text=f"formatted_{column_name}_{agg}", level='glyph',
        x_offset=-30, y_offset=0, source=source, render_mode='canvas')
    p.add_layout(labels)

    p.add_tools(HoverTool(tooltips= [("Name", "@Pcol"), ("Age", "@age"), ("Gender", "@gender"), ("Cell count", "@cell_count")], renderers=[renderer], mode='mouse'))
    p.yaxis[0].formatter = NumeralTickFormatter(format="0,0.00")

    show(p)


# # Cox 17

# ## Loading Data

# In[9]:


data_path = ".\\Data\\20181231 - Cox17 GFER\\cox mito bycell1.xls"


# In[10]:


# read the data from the file
df_cox17 = pd.read_excel(data_path, header=1)

# remove redundent column
df_cox17.drop("Target Set",axis=1, inplace=True)

# rename columns
df_cox17.columns = df_cox17.columns.str.replace("TMRE", "Cox17")
df_cox17.rename({"cox total int": "Cox17 total intensity"}, axis="columns", inplace=True)

# extract the positional data 
loc_df = df_cox17.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_cox17 = df_cox17.join(loc_df)
df_cox17.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
df_cox17.Pcol.replace([2,3,4,5,6,7,8,9,10,11], 
                ["AV", "HC CM", "EV", "AG044", "HV", "AG015", "HC IK", "MV", "HC OZ", "HC NA0730"],
               inplace=True)
df_cox17 = df_cox17.join(patients_df, on='Pcol')

df_cox17.sample(7)


# In[11]:


df_cox17.describe()


# In[12]:


cell_for_patients = df_cox17.groupby(["Prow", "Pcol"])["Pfield"].count()
cell_for_patients = cell_for_patients.unstack(level=-1)
fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(cell_for_patients, linewidths=0.5, ax=ax, annot=True, fmt="0.00f")


# In[13]:


df_cox17["Pcol"].value_counts().plot(kind='bar')


# <div class="alert alert-warning" role="warning">
#   Do note, the number of cells is not identical. <BR> Is this difference OK?
# </div>

# ## Group by patient
# 

# In[14]:


df_cox17["Cox17 area ratio*"] = df_cox17['Cox17 AREA']/df_cox17['CELL AREA']
df_cox17["Cox17 intensity ratio*"] = df_cox17["Cox17 INTENSITY"]/df_cox17['CELL INTENSITY']
df_cox17["Cox17 DxA ratio*"] = df_cox17['Cox17 DxA']/df_cox17['CELL DXA']


# In[15]:


cox17_features = df_cox17.columns[df_cox17.columns.str.contains("Cox17")].tolist()
cox17_features


# In[16]:


cox17_features.remove('Cox17 COUNT')
cox17_features.remove('Cox17 mean area')
cox17_features.remove('Cox17 mean intesity')


# In[17]:


df_grp_row = df_cox17.groupby("Pcol")

cox17_medians = df_grp_row[cox17_features].median()
display(cox17_medians)


# In[18]:


plot_column(df_grp_row, 'Cox17 AREA')


# In[19]:


plot_column(df_grp_row, 'Cox17 INTENSITY')


# In[20]:


plot_column(df_grp_row, 'Cox17 area ratio*')


# In[21]:


plot_column(df_grp_row, 'Cox17 DxA ratio*')


# In[22]:


plot_column(df_grp_row, 'Cox17 DxA')


# ## Cox17 AREA
# Cox17 AREA seems to be correlated with the cell area

# In[23]:


display(df_cox17[["Cox17 AREA", "CELL AREA"]].corr())

from matplotlib.lines import Line2D
lgn = [Line2D([0], [0], color=p['color'], lw=2, label='Line') for p in patients.values()]
plt.scatter(x=df_cox17["Cox17 AREA"], y=df_cox17["CELL AREA"], c=df_cox17["Pcol"].apply(lambda x: patients[x]['color']), alpha=0.85)
plt.legend(lgn, patients.keys())


# Very weak correlation exists 

# In[24]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 AREA",by="Pcol", ax=ax, showfliers=False )
plt.show()


# In[25]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 AREA')


# In[26]:


print("Cox17 AREA - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[27]:


corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 total area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon is significantly less then everybody expect Ella and me
# </div>

# In[28]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 AREA')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 AREA - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')
print("\n")
print("Cox17 AREA - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Ella is significantly less then all the children expect Alon and Maydan
# </div>

# In[29]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 AREA"].get_group('AV'), kde=True,bins=range(0, 700, 25), ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 AREA"].get_group('EV'), kde=True, bins=range(0, 700, 25) ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[30]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 AREA"].get_group('AV'), kde=True, bins=range(0, 700, 25), ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 AREA"].get_group('AG015'), kde=True, bins=range(0, 700, 25) ,ax=ax, color='g')
ax.legend(['AV','AG015'])


# ## Cox 17 - Cell area

# In[31]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="CELL AREA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[32]:


df_grp_row["CELL AREA"].median().plot(kind='bar')


# In[33]:


p_values = compare_results(df_cox17, 'AV', 'CELL AREA', hyp_alternative='greater')


# In[34]:


print("CELL AREA - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[35]:


corrected_pval = pval_corrected(list(p_values.values()))
print("CELL AREA - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon cell area is not significantly larger then the other children 
# </div>

# In[36]:


# Can we say the cells divide less and that's why they are bigger?
df_grp_row["CELL AREA"].sum().plot(kind='bar')


# In[ ]:





# ## Cox17 area ratio*

# In[37]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 area ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[38]:


cox17_medians = df_grp_row["Cox17 area ratio*"].median()
display(cox17_medians)


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)
# Set title
cox17_medians.plot(kind='bar', ax=ax, subplots=True, layout=(1,1))


# In[39]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 area ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 area ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[40]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 area ratio*')


# In[41]:


print("Cox17 area ratio* - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[42]:


corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 area ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon Cox17 area ratio is significantly less then the other children (except Ella)
# </div>

# In[43]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 area ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 area ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Ella Cox17 area ratio is significantly less then the other children (except Alon)
# </div>

# ## Cox17 DxA ratio*

# In[44]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 DxA ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[45]:


cox17_medians = df_grp_row["Cox17 DxA ratio*"].median()
display(cox17_medians)


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)
# Set title
cox17_medians.plot(kind='bar', ax=ax, subplots=True, layout=(1,1))


# In[46]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 DxA ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 DxA ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[47]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 DxA ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 DxA ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# In[48]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 DxA ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 DxA ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


#   

# # Cox17 and Mito

# In[49]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="MITOTRECKER AREA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[50]:


df_cox17['Mito AREA*'] = df_cox17['MITOTRECKER AREA'] / df_cox17["CELL AREA"]


# In[51]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Mito AREA*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[52]:


df_cox17['Cox17 per Mito AREA'] = df_cox17['Cox17 AREA'] / df_cox17["MITOTRECKER AREA"]


# In[53]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 per Mito AREA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[54]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="MITOTRACKER DxA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[55]:


df_cox17['Mito DxA*'] = df_cox17['MITOTRACKER DxA'] / df_cox17["CELL AREA"]


# In[56]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Mito DxA*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[57]:


df_cox17['Cox17 per Mito DxA'] = df_cox17["Cox17 DxA"]/ df_cox17['MITOTRACKER DxA'] 


# In[58]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 per Mito DxA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[103]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 per Mito DxA')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 per Mito DxA - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# In[104]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 per Mito DxA')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 per Mito DxA - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# # GFER

# ## Loading data

# In[59]:


data_path = ".\\Data\\20181231 - Cox17 GFER\\gfer mito by cell.xls"


# In[60]:


df_gfer = pd.read_excel(data_path, header=1)

df_gfer.drop("Target Set",axis=1, inplace=True)
df_gfer.columns = df_gfer.columns.str.replace("TMRE", "GFER")
loc_df = df_gfer.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_gfer = df_gfer.join(loc_df)
df_gfer.drop("Section",axis=1, inplace=True)
df_gfer.Pcol.replace([2,3,4,5,6,7,8,9,10,11], 
                ["AV", "HC CM", "EV", "AG044", "HV", "AG015", "HC IK", "MV", "HC OZ", "HC NA0730"],
               inplace=True)
df_gfer = df_gfer.join(patients_df, on='Pcol')

df_gfer.sample(7)


# In[61]:


df_gfer.describe()


# In[62]:


cell_for_patients = df_gfer.groupby(["Prow", "Pcol"])["Pfield"].count()
cell_for_patients = cell_for_patients.unstack(level=-1)
fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(cell_for_patients, linewidths=0.5, ax=ax, annot=True, fmt="0.00f")


# In[63]:


df_gfer["Pcol"].value_counts().plot(kind='bar')


# ## Group by patient

# In[64]:


df_gfer["GFER area ratio*"] = df_gfer['GFER AREA']/df_gfer['CELL AREA']
df_gfer["GFER intensity ratio*"] = df_gfer["GFER INTENSITY"]/df_gfer['CELL INTENSITY']
df_gfer["GFER DxA ratio*"] = df_gfer["GFER DxA"]/df_gfer['CELL DXA']


# In[65]:


df_grp_gfer_row = df_gfer.groupby("Pcol")
GFER_features = df_gfer.columns[df_gfer.columns.str.contains("GFER")].tolist()
GFER_features


# In[66]:


GFER_features.remove('GFER COUNT')
GFER_features.remove('GFER mean area')
GFER_features.remove('GFER mean intesity')


# In[67]:


GFER_medians = df_grp_gfer_row[GFER_features].median()
display(GFER_medians)


# In[68]:


plot_column(df_grp_gfer_row, 'GFER AREA', agg='50%')


# In[69]:


plot_column(df_grp_gfer_row, 'GFER area ratio*')


# In[70]:


plot_column(df_grp_gfer_row, 'GFER DxA ratio*')


# In[71]:


plot_column(df_grp_gfer_row, 'GFER INTENSITY')


# In[72]:


plot_column(df_grp_gfer_row, 'GFER intensity ratio*')


# In[73]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_gfer_row["GFER area ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_gfer_row["GFER area ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
sns.distplot(df_grp_gfer_row["GFER area ratio*"].get_group('AG015'), kde=True ,ax=ax, color='g')

ax.legend(['AV','EV', 'AG015'])


# In[74]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER area ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[75]:


p_values = compare_results(df_gfer, 'AV', 'GFER area ratio*', hyp_alternative='less')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon GFER area ratio* is significantly less than the other children (except Ella) but not the adults
# </div>

# ## GFER DxA ratio*

# In[76]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER DxA ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[77]:


p_values = compare_results(df_gfer, 'AV', 'GFER DxA ratio*', hyp_alternative='less')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon GFER DxA ratio* is <b> not </b> significantly lower then the rest of the children.
# </div>

# In[78]:


p_values = compare_results(df_gfer, 'EV', 'GFER DxA ratio*', hyp_alternative='less')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Ella GFER DxA ratio* is significantly lower then the rest of the children including Alon.
# </div>

# In[79]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="MITOTRACKER DxA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[80]:


df_gfer['GFER per Mito DxA'] = df_gfer["GFER DxA"]/ df_gfer['MITOTRACKER DxA'] 
df_gfer['GFER per Mito AREA'] = df_gfer['GFER AREA'] / df_gfer["MITOTRECKER AREA"]


# In[81]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER per Mito DxA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[82]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER per Mito AREA",by="Pcol", ax=ax, showfliers=False)
plt.show()


# # Effecting Factors

# ## Age

# ### Cox17

# In[83]:


df_cox17["Age_binned"]=pd.cut(df_cox17.age, [0,12*12*4,21*12*4,80*12*4], labels=['Child', 'Adolescent', 'Adult'])


# In[84]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 DxA ratio*",by="Age_binned", ax=ax, showfliers=False)
plt.show()


# In[85]:


df_grp_age = df_cox17.groupby("Age_binned")
child_data = df_grp_age["Cox17 DxA ratio*"].get_group('Child')
adult_data = df_grp_age["Cox17 DxA ratio*"].get_group('Adult')
stat, p = mannwhitneyu(child_data, adult_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ### GFER

# In[86]:


df_gfer["Age_binned"]=pd.cut(df_gfer.age, [0,12*12*4,21*12*4,80*12*4], labels=['Child', 'Adolescent', 'Adult'])


# In[87]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER DxA ratio*",by="Age_binned", ax=ax, showfliers=False)
plt.show()


# In[88]:


df_grp_age = df_gfer.groupby("Age_binned")
child_data = df_grp_age["GFER DxA ratio*"].get_group('Child')
adult_data = df_grp_age["GFER DxA ratio*"].get_group('Adult')
stat, p = mannwhitneyu(child_data, adult_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ## Gender

# ### Cox17

# In[89]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 DxA ratio*",by="gender", ax=ax, showfliers=False)
plt.show()


# In[90]:


female_data = df_cox17.loc[df_cox17.gender=="F","Cox17 DxA ratio*"]
male_data = df_cox17.loc[df_cox17.gender=="M","Cox17 DxA ratio*"]
stat, p = mannwhitneyu(female_data, male_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ### GFER

# In[91]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER DxA ratio*",by="gender", ax=ax, showfliers=False)
plt.show()


# In[92]:


female_data = df_gfer.loc[df_gfer.gender=="F","GFER DxA ratio*"]
male_data = df_gfer.loc[df_gfer.gender=="M","GFER DxA ratio*"]
stat, p = mannwhitneyu(female_data, male_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ## Conclusions
# Female have more GFER and COX then Male   
# Children have more GFER and COX then adults

# # Data Comparisons
# <div class="alert alert-warning" role="alert">
#     NOTE: This section is still a <b>WORK IN PROGRESS</b> !!!
# </div>
# Cox17 and GFER are on the same plate and it should be possible to prove they are the same (assuming the different antibodies does not effect external parameters differently).  
# Lets check inside each parameter for each person does the distribution is similar between "plates"
# 

# In[93]:


# Cox17 Cell area median
df_grp_row = df_cox17.groupby("Pcol")

plot_column(df_grp_row, 'CELL AREA')


# In[94]:


# GFER Cell area median
plot_column(df_grp_gfer_row, 'CELL AREA')


# The general pattern, by medians, look almost the same. Can this be translated into the by cell view?

# In[95]:


ax = df_cox17.boxplot(column="CELL AREA",by="Pcol", return_type='axes', showfliers=False)

boxprops = dict(linestyle='-', linewidth=2.5 )
medianprops = dict(linestyle='-', linewidth=2.5)
df_gfer.boxplot(column="CELL AREA",by="Pcol", ax=ax, boxprops=boxprops,medianprops=medianprops, showfliers=False )
plt.show()


# The bold lines belong to GFER while the other is Cox17.  
# Normalization by one of the patients doesn't look good as for instance *HC IK* and *HC NA0730* differences are in opposite direction.

# In[96]:


patients_names = df_cox17.Pcol.unique()
for patient in patients_names:
    stat, p = mannwhitneyu(df_cox17.loc[df_cox17.Pcol==patient, 'CELL AREA'], df_gfer.loc[df_gfer.Pcol==patient, 'CELL AREA'], alternative='two-sided')
    print(f"{patient}: {p:0.00}")


# The p value is 0.05 or above and there distributions cannot be considered as different but for some the distance is quite big.  
# Maybe Kolmogorov-Smirnov test will help us? It have null hypothesis that 2 independent samples are drawn from the same continuous distribution.

# In[97]:


from scipy import stats

patients_names = df_cox17.Pcol.unique()
for patient in patients_names:
    stat, p = stats.ks_2samp(df_cox17.loc[df_cox17.Pcol==patient, 'CELL AREA'], df_gfer.loc[df_gfer.Pcol==patient, 'CELL AREA'])
    print(f"{patient}: {p:0.00}")


# In[98]:


data_comp_ks = pd.DataFrame()
for feature in df_cox17.columns:
    if feature not in df_gfer.columns:
        continue
    if feature in ['Target', "Cell cycle", "Toxicity status", "Cell status", "age", "gender", "color", "Pcol", "Prow", "Pfield", "Age_binned"]:
        continue
    for patient in patients_names:
        stat, p = stats.ks_2samp(df_cox17.loc[df_cox17.Pcol==patient, feature], df_gfer.loc[df_gfer.Pcol==patient, feature]) 
        data_comp_ks.loc[feature, patient] = p
data_comp_ks[data_comp_ks<1e-4] = 0
data_comp_ks


# In[99]:


fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(data_comp_ks, linewidths=0.5, ax=ax, annot=True)


# There is no row without black this means we cannot say the data came from the same distributions.  
# Maybe a multiple comparison adjustment will help but I am not sure it fits here.  
# Probably more data would had helped.  
# Maybe alignment of the data would help?

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




