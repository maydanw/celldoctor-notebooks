#!/usr/bin/env python
# coding: utf-8

# # GFER-DRP1 20181212
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports
# 

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()


# In[5]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# In[6]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 36.5*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": 53*12*4, "gender": "M", "color": "coral"}}
patients_df = pd.DataFrame(patients).T
patients_df


# In[7]:


col_map = {2:"AV", 3:"EV", 4:"HC CM", 5:"AG044", 6:"AG015", 7:"HV", 8:"MV", 9:"HC IK", 10:"HC OZ", 11:"HC NA0730"}
col_map


# In[8]:


from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource, CategoricalColorMapper, LabelSet
from bokeh.palettes import Set3
from bokeh.palettes import d3
from bokeh.models.tools import HoverTool
from bokeh.models import NumeralTickFormatter


def plot_column(df, column_name, agg='50%'):

    TOOLS='pan,wheel_zoom,box_zoom,reset'

    source = ColumnDataSource(df)
    patients_names =  source.data['Pcol'].tolist()
    plot_options = dict(width=750, plot_height=350,tools=TOOLS)
    p = figure(**plot_options, x_range=patients_names)


    # palette = d3['Category10'][len(patients_names)]
    # palette = [patients[pn]['color'] for pn in patients_names]
    # color_map = CategoricalColorMapper(factors=patients_names, palette=palette)
    
    source.data['color'] = [patients[pn]['color'] for pn in patients_names]
    source.data['age'] = [patients[pn]['age']/4/12 for pn in patients_names]
    source.data['gender'] = [patients[pn]['gender'] for pn in patients_names]
    
    cell_count = df["Pcol"].count().to_dict()
    source.data['cell_count'] = [cell_count[pn] for pn in patients_names]

    # renderer = p.vbar(x='Pcol', top='Cox17 IxA_50%', width=0.95, line_color="gray", source=source, color={'field': 'Pcol', 'transform': color_map})
    renderer = p.vbar(x='Pcol', top=f"{column_name}_{agg}", width=0.95, line_color="gray", source=source, color='color')
    
    source.data[f"formatted_{column_name}_{agg}"] = [f"{x:,.4f}" if x<10 else f"{x:,.0f}" for x in source.data[f"{column_name}_{agg}"]]
    labels = LabelSet(x='Pcol', y=f"{column_name}_{agg}", text=f"formatted_{column_name}_{agg}", level='glyph',
        x_offset=-30, y_offset=0, source=source, render_mode='canvas')
    p.add_layout(labels)

    p.add_tools(HoverTool(tooltips= [("Name", "@Pcol"), ("Age", "@age"), ("Gender", "@gender"), ("Cell count", "@cell_count")], renderers=[renderer], mode='mouse'))
    p.yaxis[0].formatter = NumeralTickFormatter(format="0,0.00")

    show(p)


# In[ ]:





# # Drp1

# ## Loading Data

# In[9]:


data_path = ".\\Data\\GFER-DRP1 20181212\\COX SEG. Prtocol221118.2018.XLS"


# In[10]:


df_drp1 = pd.read_excel(data_path, sheet_name='mito_cell_nuc', header=1)

# remove redundent column
df_drp1.drop("Target",axis=1, inplace=True)

# rename columns
df_drp1.columns = df_drp1.columns.str.replace("Mitochondrial", "Drp1")
df_drp1.rename({"cox total int": "Drp1 total intensity"}, axis="columns", inplace=True)

# extract the positional data 
loc_df = df_drp1.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_drp1 = pd.concat([df_drp1, loc_df], axis=1)
df_drp1.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
df_drp1.Pcol.replace([2,3,4,5,6,7,8,9,10,11], 
                ["AV", "HC CM", "EV", "AG044", "HV", "AG015", "HC IK", "MV", "HC OZ", "HC NA0730"],
               inplace=True)
df_drp1 = df_drp1.join(patients_df, on='Pcol')

df_drp1.sample(7)


# In[11]:


df_drp1.describe()


# In[12]:


cell_in_well = df_drp1.groupby(["Prow", "Pcol"])["Pfield"].count()
cell_in_well = cell_in_well.unstack(level=-1)

cell_in_well = cell_in_well.rename(col_map, axis=1)

display(sns.heatmap(cell_in_well, linewidths=0.5,annot=True, fmt="0.00f"))


# In[13]:


df_drp1 = df_drp1[df_drp1["Prow"]!='A']


# ## Group by patient
# 

# In[15]:


df_drp1["Drp1 area ratio*"] = df_drp1['Drp1 total area']/df_drp1['Cell area']
df_drp1["Drp1 total intensity ratio*"] = df_drp1["Drp1 total intensity"]/df_drp1['Cell intensity']
df_drp1["Drp1 IxA ratio*"] = df_drp1["Drp1 IxA"]/df_drp1['Cell IxA']


# In[16]:


drp1_features = df_drp1.columns[df_drp1.columns.str.contains("Drp1")].tolist()
drp1_features.remove('Drp1 count')
drp1_features.remove('Drp1 mean area')
drp1_features.remove('Drp1 mean intensity')


# In[17]:


df_grp_row = df_drp1.groupby("Pcol")

drp1_medians = df_grp_row[drp1_features].median()
display(drp1_medians)


# In[18]:


plot_column(df_grp_row, 'Drp1 total area')


# In[19]:


plot_column(df_grp_row, 'Drp1 total intensity')


# In[20]:


plot_column(df_grp_row, 'Drp1 area ratio*')


# In[21]:


plot_column(df_grp_row, 'Drp1 IxA')


# In[22]:


plot_column(df_grp_row, 'Drp1 IxA ratio*')


# In[47]:


for feature in drp1_features:
    df_drp1.boxplot(column=feature, by='Pcol', showfliers=False)


# In[ ]:





# In[ ]:




