
# coding: utf-8

# # GFER-ROS 20181217
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[6]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports
# 

# In[7]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[8]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[9]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()


# In[10]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# In[44]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"}}
patients_df = pd.DataFrame(patients).T
patients_df


# In[45]:


col_map = {2:"AV", 3:"AV", 4:"AV", 5:"EV", 6:"EV", 7:"EV", 8:"AG015", 9:"AG015", 10:"CM", 11:"CM"}
col_map


# In[46]:


from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource, CategoricalColorMapper, LabelSet
from bokeh.palettes import Set3
from bokeh.palettes import d3
from bokeh.models.tools import HoverTool
from bokeh.models import NumeralTickFormatter


def plot_column(df, column_name, agg='50%'):

    TOOLS='pan,wheel_zoom,box_zoom,reset'

    source = ColumnDataSource(df)
    patients_names =  source.data['Pcol'].tolist()
    plot_options = dict(width=750, plot_height=350,tools=TOOLS)
    p = figure(**plot_options, x_range=patients_names)


    # palette = d3['Category10'][len(patients_names)]
    # palette = [patients[pn]['color'] for pn in patients_names]
    # color_map = CategoricalColorMapper(factors=patients_names, palette=palette)
    
    source.data['color'] = [patients[pn]['color'] for pn in patients_names]
    source.data['age'] = [patients[pn]['age']/4/12 for pn in patients_names]
    source.data['gender'] = [patients[pn]['gender'] for pn in patients_names]
    
    cell_count = df["Pcol"].count().to_dict()
    source.data['cell_count'] = [cell_count[pn] for pn in patients_names]

    # renderer = p.vbar(x='Pcol', top='Cox17 IxA_50%', width=0.95, line_color="gray", source=source, color={'field': 'Pcol', 'transform': color_map})
    renderer = p.vbar(x='Pcol', top=f"{column_name}_{agg}", width=0.95, line_color="gray", source=source, color='color')
    
    source.data[f"formatted_{column_name}_{agg}"] = [f"{x:,.4f}" if x<10 else f"{x:,.0f}" for x in source.data[f"{column_name}_{agg}"]]
    labels = LabelSet(x='Pcol', y=f"{column_name}_{agg}", text=f"formatted_{column_name}_{agg}", level='glyph',
        x_offset=-30, y_offset=0, source=source, render_mode='canvas')
    p.add_layout(labels)

    p.add_tools(HoverTool(tooltips= [("Name", "@Pcol"), ("Age", "@age"), ("Gender", "@gender"), ("Cell count", "@cell_count")], renderers=[renderer], mode='mouse'))
    p.yaxis[0].formatter = NumeralTickFormatter(format="0,0.00")

    show(p)


# # ROS

# ## Loading Data

# In[47]:


data_path = ".\\Data\\ros SEG. Prtocol221118.2018.12.13.00.00.11.XLS"


# In[48]:


df_ros.Pcol.map(col_map)


# In[49]:


df_ros = pd.read_excel(data_path, sheet_name='mito_cell_nuc', header=1)

# remove redundent column
df_ros.drop("Target",axis=1, inplace=True)

# rename columns
df_ros.columns = df_ros.columns.str.replace("Mitochondrial", "ROS")
df_ros.rename({"cox total int": "ROS total intensity"}, axis="columns", inplace=True)

# extract the positional data 
loc_df = df_ros.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_ros = pd.concat([df_ros, loc_df], axis=1)
df_ros.drop("Section",axis=1, inplace=True)

# # replace rows with patients names and join patients data
df_ros['Pcol'] = df_ros.Pcol.map(col_map)
df_ros = df_ros.join(patients_df, on='Pcol')

df_ros.sample(7)


# In[50]:


df_ros.describe()


# In[51]:


cell_in_well = df_ros.groupby(["Prow", "Pcol"])["Pfield"].count()
cell_in_well = cell_in_well.unstack(level=-1)

cell_in_well = cell_in_well.rename(col_map, axis=1)

display(sns.heatmap(cell_in_well, linewidths=0.5,annot=True, fmt="0.00f"))


# ## Group by patient
# 

# In[52]:


df_ros["ROS area ratio*"] = df_ros['ROS total area']/df_ros['Cell area']
df_ros["ROS total intensity ratio*"] = df_ros["ROS total intensity"]/df_ros['Cell intensity']
df_ros["ROS IxA ratio*"] = df_ros["ROS IxA"]/df_ros['Cell IxA']


# In[53]:


ros_features = df_ros.columns[df_ros.columns.str.contains("ROS")].tolist()
ros_features.remove('ROS count')
ros_features.remove('ROS mean area')
ros_features.remove('ROS mean intensity')


# In[54]:


df_grp_row = df_ros.groupby("Pcol")

ros_medians = df_grp_row[ros_features].median()
display(ros_medians)


# In[55]:


plot_column(df_grp_row, 'ROS total area')


# In[56]:


plot_column(df_grp_row, 'ROS total intensity')


# In[57]:


plot_column(df_grp_row, 'ROS area ratio*')


# In[58]:


plot_column(df_grp_row, 'ROS IxA')


# In[59]:


plot_column(df_grp_row, 'ROS IxA ratio*')


# In[60]:


for feature in ros_features:
    df_ros.boxplot(column=feature, by='Pcol', showfliers=False)

