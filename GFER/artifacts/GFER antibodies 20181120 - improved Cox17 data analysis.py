#!/usr/bin/env python
# coding: utf-8

# # GFER with antibodies analysis with improved Cox17 data analysis
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

# Do run in the first time: jupyter nbextension enable --py --sys-prefix widgetsnbextension


# In[4]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# In[5]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 36.5*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": 53*12*4, "gender": "M", "color": "coral"}}
patients_df = pd.DataFrame(patients).T
patients_df


# In[6]:


from scipy.stats import mannwhitneyu

def compare_results(df, base_key, feature_name, df_grpby_key='Pcol', hyp_alternative="less"):
    df_grp_row = df.groupby(df_grpby_key)
    base_data = df_grp_row[feature_name].get_group(base_key)

    p_values = {}
    for grp_k in df_grp_row[feature_name].groups.keys():
        if grp_k==base_key:
            continue
        p_data = df_grp_row[feature_name].get_group(grp_k)
        stat, p = mannwhitneyu(base_data, p_data, alternative=hyp_alternative)
        p_values[grp_k] = p
    return p_values


# In[7]:


def pval_corrected(pvals_raw, method=None):
    '''p-values corrected for multiple testing problem
 
    This uses the default p-value correction of the instance stored in
    ``self.multitest_method`` if method is None.
 
    '''
    import statsmodels.stats.multitest as smt
    if method is None:
        method = 'Bonferroni'
    #TODO: breaks with method=None
    return smt.multipletests(pvals_raw, method=method)[1]


# In[8]:


from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource, CategoricalColorMapper, LabelSet
from bokeh.palettes import Set3
from bokeh.palettes import d3
from bokeh.models.tools import HoverTool
from bokeh.models import NumeralTickFormatter


def plot_column(df, column_name, agg='50%'):

    TOOLS='pan,wheel_zoom,box_zoom,reset'

    source = ColumnDataSource(df)
    patients_names =  source.data['Pcol'].tolist()
    plot_options = dict(width=750, plot_height=350,tools=TOOLS)
    p = figure(**plot_options, x_range=patients_names)


    # palette = d3['Category10'][len(patients_names)]
    # palette = [patients[pn]['color'] for pn in patients_names]
    # color_map = CategoricalColorMapper(factors=patients_names, palette=palette)
    
    source.data['color'] = [patients[pn]['color'] for pn in patients_names]
    source.data['age'] = [patients[pn]['age']/4/12 for pn in patients_names]
    source.data['gender'] = [patients[pn]['gender'] for pn in patients_names]
    
    cell_count = df["Pcol"].count().to_dict()
    source.data['cell_count'] = [cell_count[pn] for pn in patients_names]

    # renderer = p.vbar(x='Pcol', top='Cox17 IxA_50%', width=0.95, line_color="gray", source=source, color={'field': 'Pcol', 'transform': color_map})
    renderer = p.vbar(x='Pcol', top=f"{column_name}_{agg}", width=0.95, line_color="gray", source=source, color='color')
    
    source.data[f"formatted_{column_name}_{agg}"] = [f"{x:,.4f}" if x<10 else f"{x:,.0f}" for x in source.data[f"{column_name}_{agg}"]]
    labels = LabelSet(x='Pcol', y=f"{column_name}_{agg}", text=f"formatted_{column_name}_{agg}", level='glyph',
        x_offset=-30, y_offset=0, source=source, render_mode='canvas')
    p.add_layout(labels)

    p.add_tools(HoverTool(tooltips= [("Name", "@Pcol"), ("Age", "@age"), ("Gender", "@gender"), ("Cell count", "@cell_count")], renderers=[renderer], mode='mouse'))
    p.yaxis[0].formatter = NumeralTickFormatter(format="0,0.00")

    show(p)


# # Cox 17

# ## Loading Data

# In[9]:


data_path = ".\\Data\\20181120\\Cox17-new.xls"


# In[10]:


# read the data from the file
df_cox17 = pd.read_excel(data_path, header=1)

# remove redundent column
df_cox17.drop("Target Set",axis=1, inplace=True)

# rename columns
df_cox17.columns = df_cox17.columns.str.replace("Mitochondrial", "Cox17")
df_cox17.rename({"cox total int": "Cox17 total intensity"}, axis="columns", inplace=True)

# extract the positional data 
loc_df = df_cox17.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_cox17 = df_cox17.join(loc_df)
df_cox17.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
df_cox17.Pcol.replace([2,3,4,5,6,7,8,9,10,11], 
                ["AV", "HC CM", "EV", "AG044", "HV", "AG015", "HC IK", "MV", "HC OZ", "HC NA0730"],
               inplace=True)
df_cox17 = df_cox17.join(patients_df, on='Pcol')

df_cox17.sample(7)


# In[11]:


df_cox17.describe()


# In[12]:


df_cox17["Pcol"].value_counts().plot(kind='bar')


# <div class="alert alert-warning" role="warning">
#   Do note, the number of cells is not identical. <BR> Is this difference OK?
# </div>

# ## Group by patient
# 

# In[13]:


df_cox17["Cox17 area ratio*"] = df_cox17['Cox17 total area']/df_cox17['Cell area']
df_cox17["Cox17 total intensity ratio*"] = df_cox17["Cox17 total intensity"]/df_cox17['Cell intensity']
df_cox17["Cox17 IxA ratio*"] = df_cox17["Cox17 IxA"]/df_cox17['Cell IxA']


# In[14]:


cox17_features = df_cox17.columns[df_cox17.columns.str.contains("Cox17")].tolist()
cox17_features.remove('Cox17 count')
cox17_features.remove('Cox17 mean area')
cox17_features.remove('Cox17 mean intensity')


# In[15]:


df_grp_row = df_cox17.groupby("Pcol")

cox17_medians = df_grp_row[cox17_features].median()
display(cox17_medians)


# In[16]:


plot_column(df_grp_row, 'Cox17 total area')


# In[17]:


plot_column(df_grp_row, 'Cox17 total intensity')


# In[18]:


plot_column(df_grp_row, 'Cox17 area ratio*')


# In[19]:


plot_column(df_grp_row, 'Cox17 IxA ratio*')


# In[20]:


plot_column(df_grp_row, 'Cox17 IxA')


# ## Cox17 total area
# Cox17 total area seems to be correlated with the cell area

# In[21]:


display(df_cox17[["Cox17 total area", "Cell area"]].corr())

from matplotlib.lines import Line2D
lgn = [Line2D([0], [0], color=p['color'], lw=2, label='Line') for p in patients.values()]
plt.scatter(x=df_cox17["Cox17 total area"], y=df_cox17["Cell area"], c=df_cox17["Pcol"].apply(lambda x: patients[x]['color']), alpha=0.85)
plt.legend(lgn, patients.keys())


# Some correlation exists but it is not strong as the the variance is huge

# In[22]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 total area",by="Pcol", ax=ax, showfliers=False )
plt.show()


# In[23]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 total area')


# In[24]:


print("Cox17 total area - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[25]:


corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 total area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon is significantly less then everybody expect Ella
# </div>

# In[26]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 total area')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 total area - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')
print("\n")
print("Cox17 total area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Ella is significantly less then all the children (expect Alon) and some of the adults. With the Bonferroni correction all 
# </div>

# In[27]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 total area"].get_group('AV'), kde=False,bins=range(0, 700, 25), ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 total area"].get_group('EV'), kde=False, bins=range(0, 700, 25) ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[28]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 total area"].get_group('AV'), kde=False, bins=range(0, 700, 25), ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 total area"].get_group('HC NA0730'), kde=False, bins=range(0, 700, 25) ,ax=ax, color='g')
ax.legend(['AV','HC NA0730'])


# ## Cox 17 - Cell area

# In[29]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cell area",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[30]:


df_grp_row["Cell area"].median().plot(kind='bar')


# In[31]:


p_values = compare_results(df_cox17, 'AV', 'Cell area', hyp_alternative='greater')


# In[32]:


print("Cell area - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[33]:


corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon cell area is significantly larger then the other children (except Ella)
# </div>

# ## Cox17 area ratio*

# In[34]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 area ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[35]:


cox17_medians = df_grp_row["Cox17 area ratio*"].median()
display(cox17_medians)


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)
# Set title
cox17_medians.plot(kind='bar', ax=ax, subplots=True, layout=(1,1))


# In[36]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 area ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 area ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[37]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 area ratio*')


# In[38]:


print("Cox17 area ratio* - p values:\n"+"-"*35)
for k,v in p_values.items():
    print(f'{k}:\t\t {v:.6f}')


# In[39]:


corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 area ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon Cox17 area ratio is significantly less then the other children (except Ella)
# </div>

# In[40]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 area ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 area ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Ella Cox17 area ratio is significantly less then the other children (except Alon)
# </div>

# ## Cox17 IxA ratio*

# In[82]:


# Create a figure of given size
fig = plt.figure(figsize=(12,7))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 IxA ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[42]:


cox17_medians = df_grp_row["Cox17 IxA ratio*"].median()
display(cox17_medians)


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)
# Set title
cox17_medians.plot(kind='bar', ax=ax, subplots=True, layout=(1,1))


# In[43]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_row["Cox17 IxA ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_row["Cox17 IxA ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
ax.legend(['AV','EV'])


# In[44]:


p_values = compare_results(df_cox17, 'AV', 'Cox17 IxA ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 IxA ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# In[45]:


p_values = compare_results(df_cox17, 'EV', 'Cox17 IxA ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 IxA ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# Does AG044 is significantly different from HC CM?

# In[46]:


p_values = compare_results(df_cox17, 'AG044', 'Cox17 IxA ratio*')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cox17 IxA ratio* - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# AG044 - HC CM is the only significant difference he have.

# # GFER

# ## Loading data

# In[47]:


data_path = ".\\Data\\20181120\\GFER-new.xls"


# In[48]:


df_gfer = pd.read_excel(data_path, header=1)

df_gfer.drop("Target Set",axis=1, inplace=True)
df_gfer.columns = df_gfer.columns.str.replace("Mitochondrial", "GFER")
loc_df = df_gfer.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_gfer = df_gfer.join(loc_df)
df_gfer.drop("Section",axis=1, inplace=True)
df_gfer.Pcol.replace([2,3,4,5,6,7,8,9,10,11], 
                ["AV", "HC CM", "EV", "AG044", "HV", "AG015", "HC IK", "MV", "HC OZ", "HC NA0730"],
               inplace=True)
df_gfer = df_gfer.join(patients_df, on='Pcol')
df_gfer.rename({"cox total int": "GFER total intensity"}, axis="columns", inplace=True)

df_gfer.sample(7)


# In[49]:


df_gfer.describe()


# In[50]:


df_gfer["Pcol"].value_counts().plot(kind='bar')


# ## Group by patient

# In[51]:


df_gfer["GFER area ratio*"] = df_gfer['GFER total area']/df_gfer['Cell area']
df_gfer["GFER total intensity ratio*"] = df_gfer["GFER total intensity"]/df_gfer['Cell intensity']
df_gfer["GFER IxA ratio*"] = df_gfer["GFER IxA"]/df_gfer['Cell IxA']


# In[52]:


df_grp_gfer_row = df_gfer.groupby("Pcol")
GFER_features = df_gfer.columns[df_gfer.columns.str.contains("GFER")].tolist()
GFER_features.remove('GFER count')
GFER_features.remove('GFER mean area')
GFER_features.remove('GFER mean intensity')


# In[53]:


GFER_medians = df_grp_gfer_row[GFER_features].median()
display(GFER_medians)


# In[54]:


plot_column(df_grp_gfer_row, 'GFER total area', agg='50%')


# In[55]:


plot_column(df_grp_gfer_row, 'GFER area ratio*')


# In[56]:


plot_column(df_grp_gfer_row, 'GFER IxA ratio*')


# In[57]:


plot_column(df_grp_gfer_row, 'GFER total intensity')


# In[58]:


plot_column(df_grp_gfer_row, 'GFER total intensity ratio*')


# In[59]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_grp_gfer_row["GFER area ratio*"].get_group('AV'), kde=True, ax=ax, color='b')
sns.distplot(df_grp_gfer_row["GFER area ratio*"].get_group('EV'), kde=True ,ax=ax, color='r')
ax.legend(['AV','EV'])


# ## GFER IxA ratio*

# In[60]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER IxA ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# In[61]:


p_values = compare_results(df_gfer, 'AV', 'GFER IxA ratio*', hyp_alternative='less')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# In[62]:


p_values = compare_results(df_gfer, 'EV', 'GFER IxA ratio*', hyp_alternative='less')
corrected_pval = pval_corrected(list(p_values.values()))
print("Cell area - adjusted p values:\n"+"-"*40)
for k,v in zip(p_values.keys(), corrected_pval):
    print(f'{k}:\t\t {v:.6f}')


# <div class="alert alert-info" role="alert">
#   Alon GFER IxA ratio* is not informative but Ella's GFER IxA ratio* is significantly lower then the rest of the children.
# </div>

# ### Other measures

# In[80]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER area ratio*",by="Pcol", ax=ax, showfliers=False)
plt.show()


# # Effecting Factors

# ## Age

# ### Cox17

# In[63]:


df_cox17["Age_binned"]=pd.cut(df_cox17.age, [0,12*12*4,21*12*4,80*12*4], labels=['Child', 'Adolescent', 'Adult'])


# In[64]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 IxA ratio*",by="Age_binned", ax=ax, showfliers=False)
plt.show()


# In[65]:


df_grp_age = df_cox17.groupby("Age_binned")
child_data = df_grp_age["Cox17 IxA ratio*"].get_group('Child')
adult_data = df_grp_age["Cox17 IxA ratio*"].get_group('Adult')
stat, p = mannwhitneyu(child_data, adult_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ### GFER

# In[66]:


df_gfer["Age_binned"]=pd.cut(df_gfer.age, [0,12*12*4,21*12*4,80*12*4], labels=['Child', 'Adolescent', 'Adult'])


# In[67]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER IxA ratio*",by="Age_binned", ax=ax, showfliers=False)
plt.show()


# In[68]:


df_grp_age = df_gfer.groupby("Age_binned")
child_data = df_grp_age["GFER IxA ratio*"].get_group('Child')
adult_data = df_grp_age["GFER IxA ratio*"].get_group('Adult')
stat, p = mannwhitneyu(child_data, adult_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ## Gender

# ### Cox17

# In[69]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_cox17.boxplot(column="Cox17 IxA ratio*",by="gender", ax=ax, showfliers=False)
plt.show()


# In[70]:


female_data = df_cox17.loc[df_cox17.gender=="F","Cox17 IxA ratio*"]
male_data = df_cox17.loc[df_cox17.gender=="M","Cox17 IxA ratio*"]
stat, p = mannwhitneyu(female_data, male_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ### GFER

# In[71]:


# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

df_gfer.boxplot(column="GFER IxA ratio*",by="gender", ax=ax, showfliers=False)
plt.show()


# In[72]:


female_data = df_gfer.loc[df_gfer.gender=="F","GFER IxA ratio*"]
male_data = df_gfer.loc[df_gfer.gender=="M","GFER IxA ratio*"]
stat, p = mannwhitneyu(female_data, male_data, alternative="greater")
print(f"p vlaue: {p:0.5f}")


# ## Conclusions
# Female have more GFER and COX then Male   
# Children have more GFER and COX then adults

# # Data Comparisons
# <div class="alert alert-warning" role="alert">
#     NOTE: This section is still a <b>WORK IN PROGRESS</b> !!!
# </div>
# Cox17 and GFER are on the same plate and it should be possible to prove they are the same (assuming the different antibodies does not effect external parameters differently).  
# Lets check inside each parameter for each person does the distribution is similar between "plates"
# 

# In[73]:


# Cox17 Cell area median
df_grp_row = df_cox17.groupby("Pcol")

plot_column(df_grp_row, 'Cell area')


# In[74]:


# GFER Cell area median
plot_column(df_grp_gfer_row, 'Cell area')


# The general pattern, by medians, look almost the same. Can this be translated into the by cell view?

# In[75]:


ax = df_cox17.boxplot(column="Cell area",by="Pcol", return_type='axes', showfliers=False)

boxprops = dict(linestyle='-', linewidth=2.5 )
medianprops = dict(linestyle='-', linewidth=2.5)
df_gfer.boxplot(column="Cell area",by="Pcol", ax=ax, boxprops=boxprops,medianprops=medianprops, showfliers=False )
plt.show()


# The bold lines belong to GFER while the other is Cox17.  
# Normalization by one of the patients doesn't look good as for instance *HC IK* and *HC NA0730* differences are in opposite direction.

# In[76]:


patients_names = df_cox17.Pcol.unique()
for patient in patients_names:
    stat, p = mannwhitneyu(df_cox17.loc[df_cox17.Pcol==patient, 'Cell area'], df_gfer.loc[df_gfer.Pcol==patient, 'Cell area'], alternative='two-sided')
    print(f"{patient}: {p:0.00}")


# The p value is 0.05 or above and there distributions cannot be considered as different but for some the distance is quite big.  
# Maybe Kolmogorov-Smirnov test will help us? It have null hypothesis that 2 independent samples are drawn from the same continuous distribution.

# In[77]:


from scipy import stats

patients_names = df_cox17.Pcol.unique()
for patient in patients_names:
    stat, p = stats.ks_2samp(df_cox17.loc[df_cox17.Pcol==patient, 'Cell area'], df_gfer.loc[df_gfer.Pcol==patient, 'Cell area'])
    print(f"{patient}: {p:0.00}")


# While not the same both test are quite consistent and put us in problem with AG015.  
# Let's look at other variables.

# In[78]:


data_comp_ks = pd.DataFrame()
for feature in df_cox17.columns:
    if feature not in df_gfer.columns:
        continue
    if feature in ['Target', "Cell cycle", "Toxicity status", "Cell status", "age", "gender", "color", "Pcol", "Prow", "Pfield", "Age_binned"]:
        continue
    for patient in patients_names:
        stat, p = stats.ks_2samp(df_cox17.loc[df_cox17.Pcol==patient, feature], df_gfer.loc[df_gfer.Pcol==patient, feature]) 
        data_comp_ks.loc[feature, patient] = p
data_comp_ks[data_comp_ks<1e-4] = 0
data_comp_ks


# In[79]:


fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(data_comp_ks, linewidths=0.5, ax=ax, annot=True)


# There is no row without black this means we cannot say the data came from the same distributions.  
# Maybe a multiple comparison adjustment will help but I am not sure it fits here.  
# Probably more data would had helped.  
# Maybe alignment of the data would help?

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




