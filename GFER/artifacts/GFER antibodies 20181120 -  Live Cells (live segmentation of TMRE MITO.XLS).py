#!/usr/bin/env python
# coding: utf-8

# # GFER Live Cells (live segmentation of TMRE MITO.XLS)
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports
# 

# In[4]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

# Do run in the first time: jupyter nbextension enable --py --sys-prefix widgetsnbextension


# In[6]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # General

# In[7]:


patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 36.5*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": 53*12*4, "gender": "M", "color": "coral"}}
patients_df = pd.DataFrame(patients).T
patients_df


# In[8]:


from scipy.stats import mannwhitneyu

def compare_results(df, base_key, feature_name, df_grpby_key='Prow', hyp_alternative="less"):
    df_grp_row = df.groupby(df_grpby_key)
    base_data = df_grp_row[feature_name].get_group(base_key)

    p_values = {}
    for grp_k in df_grp_row[feature_name].groups.keys():
        if grp_k==base_key:
            continue
        p_data = df_grp_row[feature_name].get_group(grp_k)
        stat, p = mannwhitneyu(base_data, p_data, alternative=hyp_alternative)
        p_values[grp_k] = p
    return p_values


# In[9]:


def pval_corrected(pvals_raw, method=None):
    '''p-values corrected for multiple testing problem
 
    This uses the default p-value correction of the instance stored in
    ``self.multitest_method`` if method is None.
 
    '''
    import statsmodels.stats.multitest as smt
    if method is None:
        method = 'Bonferroni'
    #TODO: breaks with method=None
    return smt.multipletests(pvals_raw, method=method)[1]


# # Load Live Cells Data

# In[10]:


data_path = ".\\Data\\20181120\\live segmentation of TMRE MITO.XLS"


# In[11]:


# read the data from the file
df_live = pd.read_excel(data_path, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)

# remove redundent column
df_live.drop("Target",axis=1, inplace=True)

# extract the positional data 
loc_df = df_live.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_live = df_live.join(loc_df)
df_live.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
mapping_series = pd.Series(["AV", "AV", "HC CM", "HC CM", "EV", "EV", "AG044", "AG044", "HV", "HV", "AG015"], index=[2,3,4,5,6,7,8,9,10,11,12])
df_live["patient"] = df_live["Pcol"].map(mapping_series)
df_live = df_live.join(patients_df, on='patient')


# In[12]:


df_live


# In[13]:


df_live.describe()


# ## Cells count

# In[14]:


cell_for_patients = df_live.groupby(["patient"])["Pfield"].count()
display(cell_for_patients)
fig, ax = plt.subplots(figsize=(10,6)) 
cell_for_patients.plot(kind='bar')


# In[15]:


cell_for_patients = df_live.groupby(["Prow", "patient"])["Pfield"].count()
cell_for_patients = cell_for_patients.unstack(level=-1)
fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(cell_for_patients, linewidths=0.5, ax=ax, annot=True, fmt="0.00f")


# In[16]:


cell_in_well = df_live.groupby(["Prow", "Pcol"])["Pfield"].count()
cell_in_well = cell_in_well.unstack(level=-1)

col_map = {2:"AV", 3:"AV", 4:"HC CM", 5:"HC CM", 6:"EV", 7:"EV", 8:"AG044", 9:"AG044", 10:"HV", 11:"HV", 12:"AG015"}
cell_in_well = cell_in_well.rename(col_map, axis=1)

fig, ax = plt.subplots(figsize=(10,6)) 
sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f")


# AG015 have only one column and fewer cells per column and therefore may not be relevant.  
# Let's see if we see a relation between the number of cells in a well and the cells area?

# In[17]:


well_cell_area = df_live.groupby(["Prow", "Pcol"])["CELL AREA"].median()
well_cell_area = well_cell_area.unstack(level=-1)

col_map = {2:"AV", 3:"AV", 4:"HC CM", 5:"HC CM", 6:"EV", 7:"EV", 8:"AG044", 9:"AG044", 10:"HV", 11:"HV", 12:"AG015"}
well_cell_area = well_cell_area.rename(col_map, axis=1)

fig, ax = plt.subplots(figsize=(10,6)) 
# sns.heatmap(well_cell_area, linewidths=0.5, ax=ax, annot=True, fmt="0.00f")
plt.scatter(x=cell_in_well, y=well_cell_area)
fig.suptitle('Cell area - Cell count relation in a well')
plt.xlabel('Number of cells')
plt.ylabel('Median of cells area')


# There seem to be no obvious relation.

# # Correcting the data

# In[18]:


# Build the plate
plate_read_order = []
col_num = len(df_live.Pcol.unique())
for r in range(len(df_live.Prow.unique())):
    if r % 2 == 0:
        plate_read_order.append(list(range(1+(r*col_num),12+(r*col_num))))
    else:
        plate_read_order.append(list(range(col_num+(r*col_num),0+(r*col_num),-1)))

plate_read_order_df = pd.DataFrame(plate_read_order)        
plate_read_order_df.set_index(df_live.Prow.unique(), inplace=True)
plate_read_order_df.columns=df_live.Pcol.unique()
plate_read_order_df


# In[19]:


def get_reading_order(row, col):
    return plate_read_order_df.loc[row, col]

def get_row_col(read_order_value):
    return 1,1

def calc_change(df, col_name, show_plot=True):
    temp_df=pd.DataFrame()
    temp_df[col_name] = df[col_name]
    temp_df["read order"] = df.apply(lambda row: get_reading_order(row["Prow"], row["Pcol"]), axis=1)

    a,b = np.polyfit(temp_df["read order"], temp_df[col_name], 1)
    x = np.linspace(temp_df["read order"].min(),temp_df["read order"].max(), len(temp_df["read order"].unique()))
    y = x*a + b
    change_percent = (y[-1]-y[0])/y[0]
    if show_plot:
        plt.scatter(x=temp_df["read order"], y=temp_df[col_name])
        plt.plot(x,y, c='r')
        plt.suptitle(col_name)
        plt.show()
    
    return change_percent, (a,b)


# In[20]:


# columns_to_align = df_live.columns[df_live.columns.str.contains('int')].tolist() +df_live.columns[df_live.columns.str.contains('INTENSITY')].tolist()
columns_to_align = ['TMRE mean int',
 'Mitotracker mean int',
 'TMRE INTENSITY',
 'MITOTRACKER INTENSITY',
 'NUCLEAR INTENSITY',
 'CELL INTENSITY',
 'CELL AREA']

for col in columns_to_align:
    change_percent,(slope,intercept) = calc_change(df_live, col)
    print(f"\'{col}\' change percent: {change_percent*100:0.2f}%")
    if abs(change_percent)>0.2:
        print(f"Correcting \'{col}\' ...")
        df_live[col+"_Corrected"] = df_live[col]-df_live.loc[:,["Prow", "Pcol"]].apply(lambda row: get_reading_order(row["Prow"], row["Pcol"]), axis=1)*slope


# In[21]:


columns_to_align = df_live.columns[df_live.columns.str.contains('_Corrected')].tolist()

for col in columns_to_align:
    change_percent,(slope,intercept) = calc_change(df_live, col)


# # Features by patients

# In[22]:


features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield',
                      'patient', 'age','color', 'gender',]
 

for feature in df_live.columns:
    if feature in features_to_ignore:
        continue
    df_live.boxplot(column=feature, by='patient', showfliers=False)


# What was the correction effect?  
# *bold line is the corrected boxplot*

# In[23]:


ax = df_live.boxplot(column='MITOTRACKER INTENSITY', by='patient', return_type='axes', showfliers=False)

boxprops = dict(linestyle='--', linewidth=2.5)
medianprops = dict(linestyle='-', linewidth=2.5)

df_live.boxplot(column="MITOTRACKER INTENSITY_Corrected",by="patient", ax=ax, boxprops=boxprops,medianprops=medianprops, showfliers=False )

plt.show()


# ### Normalized Values

# In[24]:


df_live['TMRE AREA Ratio*'] = df_live['TMRE AREA']/df_live['MITOTRECKER AREA']
df_live.boxplot(column='TMRE AREA Ratio*', by='patient', showfliers=False)


# In[25]:


df_live['MITOTRECKER AREA Ratio*'] = df_live['MITOTRECKER AREA']/df_live['CELL AREA']
df_live.boxplot(column='MITOTRECKER AREA Ratio*', by='patient', showfliers=False)


# In[26]:


df_live['TMRE AREA Ratio**'] = df_live['TMRE AREA']/df_live['CELL AREA']
df_live.boxplot(column='TMRE AREA Ratio**', by='patient', showfliers=False)


# In[27]:


df_live['TMRE DxA*'] = df_live['TMRE DxA']/df_live['MITOTRACKER DxA']
df_live.boxplot(column='TMRE DxA*', by='patient', showfliers=False)


# In[28]:


df_live['TMRE DxA**'] = df_live['TMRE DxA']/df_live['CELL DXA']
df_live.boxplot(column='TMRE DxA**', by='patient', showfliers=False)


# In[29]:


df_live['TMRE INTENSITY*'] = df_live['TMRE INTENSITY']/df_live['MITOTRACKER INTENSITY_Corrected']
df_live.boxplot(column='TMRE INTENSITY*', by='patient', showfliers=False)


# <div class="alert alert-info" role="alert">
#   From what I see live cells does not seem to show clear differences between controls and Alon or Ella  
# </div>

# In[ ]:




