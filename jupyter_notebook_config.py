# Reference: https://svds.com/jupyter-notebook-best-practices-for-data-science/
import os
from subprocess import check_call


def post_save(model, os_path, contents_manager):
    """post-save hook for converting notebooks to .py scripts"""
    if model['type'] != 'notebook':
        return  # only do this for notebooks
    d, fname = os.path.split(os_path)
    if not os.path.exists(d + '/artifacts'):
        os.mkdir(d + '/artifacts')
    check_call(['jupyter', 'nbconvert', '--to', 'script', fname, '--output-dir', d + '/artifacts'], cwd=d)
    check_call(['jupyter', 'nbconvert', '--to', 'html', fname, '--output-dir', d + '/artifacts'], cwd=d)


c.FileContentsManager.post_save_hook = post_save
