
# coding: utf-8

# # Combining plates
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')
# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))


# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns



# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

# Do run in the first time: jupyter nbextension enable --py --sys-prefix widgetsnbextension


# In[5]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# In[6]:


from scipy.stats import mannwhitneyu

def compare_results(df, base_key, feature_name, df_grpby_key='Prow', hyp_alternative="less"):
    df_grp_row = df.groupby(df_grpby_key)
    base_data = df_grp_row[feature_name].get_group(base_key)

    p_values = {}
    for grp_k in df_grp_row[feature_name].groups.keys():
        if grp_k==base_key:
            continue
        p_data = df_grp_row[feature_name].get_group(grp_k)
        stat, p = mannwhitneyu(base_data, p_data, alternative=hyp_alternative)
        p_values[grp_k] = p
    return p_values


# In[7]:


def pval_corrected(pvals_raw, method=None):
    '''p-values corrected for multiple testing problem
 
    This uses the default p-value correction of the instance stored in
    ``self.multitest_method`` if method is None.
 
    '''
    import statsmodels.stats.multitest as smt
    if method is None:
        method = 'Bonferroni'
    #TODO: breaks with method=None
    return smt.multipletests(pvals_raw, method=method)[1]


# In[8]:


patients = {"NA0298": {'passage':3, 'age': 81, "gender": "M"}, 
            "SA0108": {'passage':2, 'age': 64, "gender": "M"}, 
            "NA0769": {'passage':4, 'age': 46, "gender": "M"}, 
            "SA0159": {'passage':3, 'age': 75, "gender": "M"}, 
            "NA0795": {'passage':2, 'age': 50, "gender": "M"}, 
            "SA0499": {'passage':3, 'age': 66, "gender": "M"},
            "NA0971": {'passage':3, 'age': 56, "gender": "M"}, 
            "SA0708": {'passage':3, 'age': 53, "gender": "M"}, 
            "NA1054": {'passage':3, 'age': 51, "gender": "M"}, 
            "SA1135": {'passage':3, 'age': 43, "gender": "M"} }
patients_df = pd.DataFrame(patients).T
patients_df


# In[9]:


col_map = {2:"NA0298", 3:"SA0108", 4:"NA0769", 5:"SA0159", 6:"NA0795", 7:"SA0499", 8:"NA0971", 9:"SA0708", 10:"NA1054", 11:"SA1135"}
col_map


# # Load Data

# In[10]:


data_path1 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\tmre mito 131016 plate1.xls"
data_path2 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\tmre mito 131016 plate2.xls"
data_path3 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\tmre mito 131016 plate3.xls"


# In[11]:


# read the data from the file
df1 = pd.read_excel(data_path1, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df1["Plate"] = 1
df2 = pd.read_excel(data_path2, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df2["Plate"] = 2
df3 = pd.read_excel(data_path3, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df3["Plate"] = 3
df_all = pd.concat([df1,df2,df3])

print(f"Shape of df1: {df1.shape}")
print(f"Shape of df2: {df2.shape}")
print(f"Shape of df3: {df3.shape}")
print(f"Shape of df_all: {df_all.shape}")

del df1
del df2
del df3


# In[12]:


# remove redundent column
df_all.drop("Target",axis=1, inplace=True)

# extract the positional data 
loc_df = df_all.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_all = pd.concat([df_all, loc_df], axis=1)
df_all.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
mapping_series = pd.Series(list(col_map.values()), index=list(col_map.keys()))
df_all["patient"] = df_all["Pcol"].map(mapping_series)
df_all = df_all.join(patients_df, on='patient')

df_all


# In[13]:


axes = plt.subplots(1, 3, figsize=(20,6))

for p,ax in zip(df_all.Plate.unique(), axes[1]):
    cell_in_well = df_all[df_all.Plate==p].groupby(["Prow", "Pcol"])["Pfield"].count()
    cell_in_well = cell_in_well.unstack(level=-1)

    cell_in_well = cell_in_well.rename(col_map, axis=1)

    display(sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f"))


# In[14]:


df_all = df_all[df_all["Prow"]!='G']


# # Correcting the data

# In[15]:


# Build the plate
plate_read_order = []
col_num = len(df_all.Pcol.unique())
for r in range(len(df_all.Prow.unique())):
    if r % 2 == 0:
        plate_read_order.append(list(range(1+(r*col_num),col_num+(r*col_num)+1)))
    else:
        plate_read_order.append(list(range(col_num+(r*col_num),(r*col_num),-1)))

plate_read_order_df = pd.DataFrame(plate_read_order)        
plate_read_order_df.set_index(df_all.Prow.unique(), inplace=True)
plate_read_order_df.columns=df_all.Pcol.unique()
plate_read_order_df


# In[16]:


def get_reading_order(row, col):
    return plate_read_order_df.loc[row, col]

def get_row_col(read_order_value):
    return 1,1

def calc_change(df, col_name, show_plot=True):
    temp_df=pd.DataFrame()
    temp_df[col_name] = df[col_name]
    temp_df["read order"] = df.apply(lambda row: get_reading_order(row["Prow"], row["Pcol"]), axis=1)

    a,b = np.polyfit(temp_df["read order"], temp_df[col_name], 1)
    x = np.linspace(temp_df["read order"].min(),temp_df["read order"].max(), len(temp_df["read order"].unique()))
    y = x*a + b
    change_percent = (y[-1]-y[0])/y[0]
    if show_plot:
        plt.scatter(x=temp_df["read order"], y=temp_df[col_name])
        plt.plot(x,y, c='r')
        plt.suptitle(col_name)
        plt.show()
    
    return change_percent, (a,b)


# In[17]:


df_all.columns


# In[18]:


columns_to_align = ['TMRETRACKER INTENSITY',
 'MITO INTENSITY',
 'NUCLEAR INTENSITY',
 'CELL INTENSITY',
 'CELL AREA',
 'NUCLEAR AREA',
 'MITO COUNT',
 'TMRETRACKER COUNT']

for col in columns_to_align:
    change_percent,(slope,intercept) = calc_change(df_all, col)
    print(f"\'{col}\' change percent: {change_percent*100:0.2f}%")
    if abs(change_percent)>0.2:
        print(f"Correcting \'{col}\' ...")
        df_all[col+"_Corrected"] = df_all[col]-df_all.loc[:,["Prow", "Pcol"]].apply(lambda row: get_reading_order(row["Prow"], row["Pcol"]), axis=1)*slope


# In[19]:


columns_to_align = df_all.columns[df_all.columns.str.contains('_Corrected')].tolist()

for col in columns_to_align:
    change_percent,(slope,intercept) = calc_change(df_all, col)


# # Unification of plates

# ## Statistical tests

# ### Kolmogorov-Smirnov statistic on 2 samples

# In[20]:


from scipy import stats

patients_names = df_all.patient.unique()
feature = 'CELL AREA' 

feature_plate_unification = pd.DataFrame()
for patient1 in patients_names:
    for patient2 in patients_names:    
        p1 = df_all.loc[(df_all.Plate==1) & (df_all.patient==patient1), feature]
        p2 = df_all.loc[(df_all.Plate==2) & (df_all.patient==patient2), feature]
        stat, p_val = stats.ks_2samp(p1, p2)
        feature_plate_unification.loc[patient1,patient2] = p_val

for patient1 in patients_names:
    feature_plate_unification[patient1] = pval_corrected(feature_plate_unification[patient1].values)

for patient2 in patients_names:
    feature_plate_unification.loc[:,patient2] = pval_corrected(feature_plate_unification.loc[:,patient2].values)
feature_plate_unification = feature_plate_unification*100
# display(feature_plate_unification.style.format("{:.3%}"))
display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
plt.show()


# ### Mann-Whitney rank test

# In[21]:


feature_plate_unification = pd.DataFrame()
for patient1 in patients_names:
    for patient2 in patients_names:    
        p1 = df_all.loc[(df_all.Plate==1) & (df_all.patient==patient1), feature]
        p2 = df_all.loc[(df_all.Plate==2) & (df_all.patient==patient2), feature]
        stat, p_val = mannwhitneyu(p1, p2)
        feature_plate_unification.loc[patient1,patient2] = p_val

for patient1 in patients_names:
    feature_plate_unification[patient1] = pval_corrected(feature_plate_unification[patient1].values)

for patient2 in patients_names:
    feature_plate_unification.loc[:,patient2] = pval_corrected(feature_plate_unification.loc[:,patient2].values)

feature_plate_unification = feature_plate_unification*100
display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
plt.show()


# ### Acceptable changes

# In[22]:


P1=1
P2=2
acceptable_change = 0.10
testing_range=80


features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue
    feature_plate_unification = pd.DataFrame()
    feature_plate_unification_internal = pd.DataFrame()
    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]

            P1p1_stats = np.percentile(P1p1, [50-testing_range//2,50,50+testing_range//2])
            P1p2_stats = np.percentile(P1p2, [50-testing_range//2,50,50+testing_range//2])
            P2p2_stats = np.percentile(P2p2, [50-testing_range//2,50,50+testing_range//2])

            P1p1_P1p2_change_percent = (P1p1_stats-P1p2_stats)/np.min([P1p1_stats,P1p2_stats], axis=0)
            P1p1_P2p2_change_percent = (P1p1_stats-P2p2_stats)/np.min([P1p1_stats,P2p2_stats], axis=0)

            P1p1_P1p2_ok = np.all(np.abs(P1p1_P1p2_change_percent)<=acceptable_change)
            P1p1_P2p2_ok = np.all(np.abs(P1p1_P2p2_change_percent)<=acceptable_change)

            feature_plate_unification_internal.loc[patient1,patient2] = P1p1_P1p2_ok*1
            feature_plate_unification.loc[patient1,patient2] = P1p1_P2p2_ok*1

    # between plates patients similarity
    patient_similarity = feature_plate_unification.values.diagonal().sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = (feature_plate_unification==feature_plate_unification_internal).sum().sum()/feature_plate_unification.size

    # patient separation - TODO: fix equation
    patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# # Fixing the distributions

# In[23]:


df_all.columns


# In[24]:


patients


# In[25]:


feature = 'NUCLEAR AREA'
# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_all.loc[(df_all.Plate==1) & (df_all.patient=='NA0769'), feature], kde=True, ax=ax, color='r')
sns.distplot(df_all.loc[(df_all.Plate==2) & (df_all.patient=='NA0769'), feature], kde=True ,ax=ax, color='g')
sns.distplot(df_all.loc[(df_all.Plate==3) & (df_all.patient=='NA0769'), feature], kde=True ,ax=ax, color='b')
# sns.distplot(df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA1135'), feature], kde=True, ax=ax, color='y')
# sns.distplot(df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA1135'), feature], kde=True ,ax=ax, color='m')
# sns.distplot(df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA1135'), feature], kde=True ,ax=ax, color='c')
ax.legend(['P1p1','P2p1','P3p1'])


# ## Parametric approach

# In[26]:


from scipy import stats as st
import warnings

# Create models from data
def best_fit_distribution(data, bins=200, ax=None, up_precent=0.003):
    """Model data by finding best fit distribution to data"""
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # I removed few prolematic dist (laplace didn't generated new data) - hand't looked into why they caused a problem 
    DISTRIBUTIONS = [        
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]
    # Distributions to check
#     DISTRIBUTIONS = [        
#         st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
#         st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
#         st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
#         st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
#         st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
#         st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
#         st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
#         st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
#         st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
#         st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
#     ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)
                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))


                # identify if this distribution is better
                if best_sse > sse > 0:
                    # demanding "significant" imporovment with a penalty to the number of params
                    imp_perc = (best_sse-sse)/best_sse
                    if np.isnan(imp_perc) or imp_perc>=up_precent*len(params):
                        best_distribution = distribution
                        best_params = params
                        best_sse = sse
                        print("Updating the best distribution to: %s with SSE: %s (improvment of: %1.3f)" %(distribution.name, sse, imp_perc))
                        # if axis pass in add to plot
                        try:
                            if ax:
                                pd.Series(pdf, x).plot(ax=ax, label=distribution.name)
                        except Exception:
                            pass

        except Exception:
            pass
        if ax:
            ax.set_title("%s%s"%(best_distribution.name, best_params))         
            ax.legend()
    return (best_distribution.name, best_params)


# In[27]:


feature = 'CELL AREA'
data=df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA0499'), feature]
best_fit_distribution(data)


# In[28]:


feature = 'CELL AREA'
data=df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA0499'), feature]
best_fit_distribution(data)


# In[29]:


feature = 'CELL AREA'
data=df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA0499'), feature]
best_fit_distribution(data)

st.alpha.fit(data)


# In[30]:


feature = 'CELL AREA'
data=df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA1135'), feature]
print(st.gamma.fit(data))

data=df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA1135'), feature]
print(st.gamma.fit(data))

data=df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA1135'), feature]
print(st.gamma.fit(data))


# In[31]:


feature = 'CELL AREA'
data=df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA0499'), feature]
data = data - data.min()
print(st.gengamma.fit(data))


data=df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA0499'), feature]
data = data - data.min()
print(st.gengamma.fit(data))

data=df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA0499'), feature]
data = data - data.min()
print(st.gengamma.fit(data))


# It seems like the parametric approach does not converge.  
# When the distribution is forced the parametric change are spread and are not in selected parameters 

# ## None parametric approach

# In[32]:


# feature = 'CELL INTENSITY'
# transform_by = 'NA0298'
# percentile_range=100
# plate1=1
# plate2=3

# P1=df_all.loc[(df_all.Plate==plate1) & (df_all.patient==transform_by), feature]
# P2=df_all.loc[(df_all.Plate==plate2) & (df_all.patient==transform_by), feature]

# percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
# P1_percentiles = np.percentile(P1,percentiles)
# P2_percentiles = np.percentile(P2,percentiles)

# coeff_1 = np.polyfit(P2_percentiles,P1_percentiles,3)

# transformer = np.poly1d(coeff_1)


# In[33]:


# Avereging multiple transformations

# transform_by = 'NA0795'

# P1=df_all.loc[(df_all.Plate==1) & (df_all.patient==transform_by), feature]
# P2=df_all.loc[(df_all.Plate==2) & (df_all.patient==transform_by), feature]

# percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
# P1_percentiles = np.percentile(P1,percentiles)
# P2_percentiles = np.percentile(P2,percentiles)

# coeff_2 = np.polyfit(P2_percentiles,P1_percentiles,3)

# print(coeff_1)
# print(coeff_2)
# print((coeff_2+coeff_1)/2)
# coeff = (coeff_2+coeff_1)/2
# transformer = np.poly1d(coeff)


# In[34]:


# Using multiple patients to build the transformation

feature = 'NUCLEAR AREA'
transform_by = np.random.choice(patients_names,2, replace=False)
percentile_range=100
plate1=1
plate2=2
coeff = None
repeats = 1
sample_size=1.0
poly_deg = 2


for i in range(repeats):
    P1=df_all.loc[(df_all.Plate==plate1) & (df_all.patient.isin(transform_by)), feature].sample(frac=sample_size)
    P2=df_all.loc[(df_all.Plate==plate2) & (df_all.patient.isin(transform_by)), feature].sample(frac=sample_size)

    percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
    P1_percentiles = np.percentile(P1,percentiles)
    P2_percentiles = np.percentile(P2,percentiles)

    if coeff is None:
        coeff = np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
    else:
        coeff = coeff + np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
    
coeff=coeff/repeats

transformer = np.poly1d(coeff)


# In[35]:



feature_space = np.linspace(df_all[feature].min(), df_all[feature].max(), 250)
plt.plot(feature_space, transformer(feature_space))
plt.show()


# In[36]:


for p in patients_names:
    P1=df_all.loc[(df_all.Plate==plate1) & (df_all.patient==p), feature]
    P2=df_all.loc[(df_all.Plate==plate2) & (df_all.patient==p), feature]
    
    # Create a figure of given size
    fig = plt.figure(figsize=(12,5))
    # Add a subplot
    ax = fig.add_subplot(111)

    sns.distplot(P1, kde=True, hist=False, ax=ax, color='r')
    sns.distplot(P2, kde=True, hist=False ,ax=ax, color='g')
    sns.distplot(transformer(P2), kde=True, hist=False ,ax=ax, color='b')
    plt.suptitle(f"{feature} - {p}{'***' if p in transform_by else ''}")
    plt.show()


# Thats looks good.  
# 

# In[37]:


df_all.columns


# In[38]:


P1=1
P2=2
acceptable_change = 0.10
testing_range=80

percentile_range=94
percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
# transform_by = ['SA0159']
transform_by = np.random.choice(patients_names,2)
print(f'{transform_by}')

features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue
    # It may be that it is fair to move this into the loop and replace transform_by->patient1
    try:
        feature_plate_unification = pd.DataFrame()
        feature_plate_unification_internal = pd.DataFrame()
        P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient.isin(transform_by)), feature]
        P2p1 = df_all.loc[(df_all.Plate==P2) & (df_all.patient.isin(transform_by)), feature]
        P1_percentiles = np.percentile(P1p1,percentiles)
        P2_percentiles = np.percentile(P2p1,percentiles)
        coeff = np.polyfit(P2_percentiles,P1_percentiles,3)
        transformer = np.poly1d(coeff)    
    except:
        continue
    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]
            P2p2 = transformer(P2p2)

            P1p1_stats = np.percentile(P1p1, [50-testing_range//2,50,50+testing_range//2])
            P1p2_stats = np.percentile(P1p2, [50-testing_range//2,50,50+testing_range//2])
            P2p2_stats = np.percentile(P2p2, [50-testing_range//2,50,50+testing_range//2])

            P1p1_P1p2_change_percent = (P1p1_stats-P1p2_stats)/np.min([P1p1_stats,P1p2_stats], axis=0)
            P1p1_P2p2_change_percent = (P1p1_stats-P2p2_stats)/np.min([P1p1_stats,P2p2_stats], axis=0)

            P1p1_P1p2_ok = np.all(np.abs(P1p1_P1p2_change_percent)<=acceptable_change)
            P1p1_P2p2_ok = np.all(np.abs(P1p1_P2p2_change_percent)<=acceptable_change)

            feature_plate_unification_internal.loc[patient1,patient2] = P1p1_P1p2_ok*1
            feature_plate_unification.loc[patient1,patient2] = P1p1_P2p2_ok*1

    # between plates patients similarity
    patient_similarity = feature_plate_unification.values.diagonal().sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = (feature_plate_unification==feature_plate_unification_internal).sum().sum()/feature_plate_unification.size

    # patient separation - TODO: fix equation
    patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# ## Transforming and KS

# ### Without transformation

# In[ ]:


P1=1
P2=2
acceptable_change = 0.1
testing_range=80

percentile_range=94
percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
transform_by = ['SA0159']
# transform_by = np.random.choice(patients_names,1)
print(f'{transform_by}')

features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue

    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]

            stat, p_val = stats.ks_2samp(P1p1, P1p2)
            feature_plate_unification_internal.loc[patient1,patient2] = p_val
            
            stat, p_val = stats.ks_2samp(P1p1, P2p2)
            feature_plate_unification.loc[patient1,patient2] = p_val

    for patient1 in patients_names:
        feature_plate_unification[patient1] = pval_corrected(feature_plate_unification[patient1].values)

    for patient2 in patients_names:
        feature_plate_unification.loc[:,patient2] = pval_corrected(feature_plate_unification.loc[:,patient2].values)
 
    # between plates patients similarity
    patient_similarity = (feature_plate_unification.values.diagonal()>0.5).sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = ((feature_plate_unification>0.5)==(feature_plate_unification_internal>0.5)).sum().sum()/feature_plate_unification.size

#     # patient separation - TODO: fix equation
#     patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification*100, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%")
#     plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# ## With transformation

# In[ ]:


P1=1
P2=2
acceptable_change = 0.1
testing_range=80

percentile_range=94
percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
# transform_by = ['SA0159']
transform_by = np.random.choice(patients_names,2)
print(f'{transform_by}')

features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue
    # It may be that it is fair to move this into the loop and replace transform_by->patient1
    try:
        feature_plate_unification = pd.DataFrame()
        feature_plate_unification_internal = pd.DataFrame()
        P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient.isin(transform_by)), feature]
        P2p1 = df_all.loc[(df_all.Plate==P2) & (df_all.patient.isin(transform_by)), feature]
        P1_percentiles = np.percentile(P1p1,percentiles)
        P2_percentiles = np.percentile(P2p1,percentiles)
        coeff = np.polyfit(P2_percentiles,P1_percentiles,3)
        transformer = np.poly1d(coeff)    
    except:
        continue
    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]
            P2p2 = transformer(P2p2)

            stat, p_val = stats.ks_2samp(P1p1, P1p2)
            feature_plate_unification_internal.loc[patient1,patient2] = p_val
            
            stat, p_val = stats.ks_2samp(P1p1, P2p2)
            feature_plate_unification.loc[patient1,patient2] = p_val

    for patient1 in patients_names:
        feature_plate_unification[patient1] = pval_corrected(feature_plate_unification[patient1].values)

    for patient2 in patients_names:
        feature_plate_unification.loc[:,patient2] = pval_corrected(feature_plate_unification.loc[:,patient2].values)
 
    # between plates patients similarity
    patient_similarity = (feature_plate_unification.values.diagonal()>0.5).sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = ((feature_plate_unification>0.5)==(feature_plate_unification_internal>0.5)).sum().sum()/feature_plate_unification.size

#     # patient separation - TODO: fix equation
#     patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification*100, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%")
#     plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# # Let's look at other datasets

# In[39]:


data_path1 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\er lyso 131016 plate1.xls"
data_path2 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\er lyso 131016 plate2.xls"
data_path3 = "C:\\BioData\\General\\PlateUnifications\\ALS patients after 72h - serum\\er lyso 131016 plate3.xls"


# In[40]:


# read the data from the file
df1 = pd.read_excel(data_path1, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df1["Plate"] = 1
df2 = pd.read_excel(data_path2, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df2["Plate"] = 2
df3 = pd.read_excel(data_path3, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df3["Plate"] = 3
df_all = pd.concat([df1,df2,df3])

print(f"Shape of df1: {df1.shape}")
print(f"Shape of df2: {df2.shape}")
print(f"Shape of df3: {df3.shape}")
print(f"Shape of df_all: {df_all.shape}")

del df1
del df2
del df3


# In[41]:


# remove redundent column
df_all.drop("Target",axis=1, inplace=True)

# extract the positional data 
loc_df = df_all.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_all = pd.concat([df_all, loc_df], axis=1)
df_all.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
mapping_series = pd.Series(list(col_map.values()), index=list(col_map.keys()))
df_all["patient"] = df_all["Pcol"].map(mapping_series)
df_all = df_all.join(patients_df, on='patient')

df_all


# In[42]:


axes = plt.subplots(1, 3, figsize=(20,6))

for p,ax in zip(df_all.Plate.unique(), axes[1]):
    cell_in_well = df_all[df_all.Plate==p].groupby(["Prow", "Pcol"])["Pfield"].count()
    cell_in_well = cell_in_well.unstack(level=-1)

    cell_in_well = cell_in_well.rename(col_map, axis=1)

    display(sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f"))


# In[43]:


P1=1
P2=2
acceptable_change = 0.1
testing_range=80


features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue
    feature_plate_unification = pd.DataFrame()
    feature_plate_unification_internal = pd.DataFrame()
    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]

            P1p1_stats = np.percentile(P1p1, [50-testing_range//2,50,50+testing_range//2])
            P1p2_stats = np.percentile(P1p2, [50-testing_range//2,50,50+testing_range//2])
            P2p2_stats = np.percentile(P2p2, [50-testing_range//2,50,50+testing_range//2])

            P1p1_P1p2_change_percent = (P1p1_stats-P1p2_stats)/np.min([P1p1_stats,P1p2_stats], axis=0)
            P1p1_P2p2_change_percent = (P1p1_stats-P2p2_stats)/np.min([P1p1_stats,P2p2_stats], axis=0)

            P1p1_P1p2_ok = np.all(np.abs(P1p1_P1p2_change_percent)<=acceptable_change)
            P1p1_P2p2_ok = np.all(np.abs(P1p1_P2p2_change_percent)<=acceptable_change)

            feature_plate_unification_internal.loc[patient1,patient2] = P1p1_P1p2_ok*1
            feature_plate_unification.loc[patient1,patient2] = P1p1_P2p2_ok*1

    # between plates patients similarity
    patient_similarity = feature_plate_unification.values.diagonal().sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = (feature_plate_unification==feature_plate_unification_internal).sum().sum()/feature_plate_unification.size

    # patient separation - TODO: fix equation
    patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# In[44]:


feature ='CELL INTENSITY'
# Create a figure of given size
fig = plt.figure(figsize=(12,5))
# Add a subplot
ax = fig.add_subplot(111)

sns.distplot(df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA0499'), feature], kde=True, ax=ax, color='r')
sns.distplot(df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA0499'), feature], kde=True ,ax=ax, color='g')
sns.distplot(df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA0499'), feature], kde=True ,ax=ax, color='b')
# sns.distplot(df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA0708'), feature], kde=True ,ax=ax, color='yellow')
sns.distplot(df_all.loc[(df_all.Plate==1) & (df_all.patient=='SA1135'), feature], kde=True, ax=ax, color='y')
sns.distplot(df_all.loc[(df_all.Plate==2) & (df_all.patient=='SA1135'), feature], kde=True ,ax=ax, color='m')
sns.distplot(df_all.loc[(df_all.Plate==3) & (df_all.patient=='SA1135'), feature], kde=True ,ax=ax, color='c')
ax.legend(['P1p1','P2p1','P3p1'])


# In[45]:


# Using multiple patients to build the transformation

feature = 'CELL INTENSITY'
transform_by = np.random.choice(patients_names,2, replace=False)
percentile_range=94
plate1=1
plate2=3
coeff = None
repeats = 1
sample_size=1.0
poly_deg = 2


for i in range(repeats):
    P1=df_all.loc[(df_all.Plate==plate1) & (df_all.patient.isin(transform_by)), feature].sample(frac=sample_size)
    P2=df_all.loc[(df_all.Plate==plate2) & (df_all.patient.isin(transform_by)), feature].sample(frac=sample_size)

    percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
    P1_percentiles = np.percentile(P1,percentiles)
    P2_percentiles = np.percentile(P2,percentiles)

    if coeff is None:
        coeff = np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
    else:
        coeff = coeff + np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
    
coeff=coeff/repeats

transformer = np.poly1d(coeff)


# In[46]:


feature_space = np.linspace(df_all[feature].min(), df_all[feature].max(), 250)
plt.plot(feature_space, transformer(feature_space))
plt.show()


# In[47]:


for p in patients_names:
    P1=df_all.loc[(df_all.Plate==plate1) & (df_all.patient==p), feature]
    P2=df_all.loc[(df_all.Plate==plate2) & (df_all.patient==p), feature]
    
    # Create a figure of given size
    fig = plt.figure(figsize=(12,5))
    # Add a subplot
    ax = fig.add_subplot(111)

    sns.distplot(P1, kde=True, hist=False, ax=ax, color='r')
    sns.distplot(P2, kde=True, hist=False ,ax=ax, color='g')
    sns.distplot(transformer(P2), kde=True, hist=False ,ax=ax, color='b')
    plt.suptitle(f"{feature} - {p}{'***' if p in transform_by else ''}")
    plt.show()


# In[69]:


P1=1
P2=2
acceptable_change = 0.1
testing_range=80
poly_deg = 2

percentile_range=96
percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,percentile_range+(percentile_range%2==0))
# transform_by = ['NA0795']
# transform_by = np.random.choice(patients_names,2, replace=False)
transform_by = ['SA0108', 'NA0769']

print(f'{transform_by}')

features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']
features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

for feature in df_all.columns:
    if feature in features_to_ignore:
        continue
    # It may be that it is fair to move this into the loop and replace transform_by->patient1
    try:
        feature_plate_unification = pd.DataFrame()
        feature_plate_unification_internal = pd.DataFrame()
        P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient.isin(transform_by)), feature]
        P2p1 = df_all.loc[(df_all.Plate==P2) & (df_all.patient.isin(transform_by)), feature]
        P1_percentiles = np.percentile(P1p1,percentiles)
        P2_percentiles = np.percentile(P2p1,percentiles)
        coeff = np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
        transformer = np.poly1d(coeff)    
    except:
        continue
    for patient1 in patients_names:
        for patient2 in patients_names:    
            P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient1), feature]
            P1p2 = df_all.loc[(df_all.Plate==P1) & (df_all.patient==patient2), feature]
            
            P2p2 = df_all.loc[(df_all.Plate==P2) & (df_all.patient==patient2), feature]
            P2p2 = transformer(P2p2)

            P1p1_stats = np.percentile(P1p1, [50-testing_range//2,50,50+testing_range//2])
            P1p2_stats = np.percentile(P1p2, [50-testing_range//2,50,50+testing_range//2])
            P2p2_stats = np.percentile(P2p2, [50-testing_range//2,50,50+testing_range//2])

            P1p1_P1p2_change_percent = (P1p1_stats-P1p2_stats)/np.min([P1p1_stats,P1p2_stats], axis=0)
            P1p1_P2p2_change_percent = (P1p1_stats-P2p2_stats)/np.min([P1p1_stats,P2p2_stats], axis=0)

            P1p1_P1p2_ok = np.all(np.abs(P1p1_P1p2_change_percent)<=acceptable_change)
            P1p1_P2p2_ok = np.all(np.abs(P1p1_P2p2_change_percent)<=acceptable_change)

            feature_plate_unification_internal.loc[patient1,patient2] = P1p1_P1p2_ok*1
            feature_plate_unification.loc[patient1,patient2] = P1p1_P2p2_ok*1

    # between plates patients similarity
    patient_similarity = feature_plate_unification.values.diagonal().sum()/feature_plate_unification.values.diagonal().shape[0]

    # between plates consistency
    plates_consistency = (feature_plate_unification==feature_plate_unification_internal).sum().sum()/feature_plate_unification.size

    # patient separation - TODO: fix equation
    patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


    display(sns.heatmap(feature_plate_unification, linewidths=0.5,  annot=True, fmt="0.000f"))
    plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
    plt.show()


# # Comparing differences between control and ALS patient 
# ## Without transformation

# In[54]:


df_all.boxplot(column="LYSO AREA", by=['patient','Plate'], showfliers=False, rot=90)


# In[68]:


P1 = 1
P2 = 2
P3 = 3 
transform_by = ['SA0108', 'NA0769']
feature = "LYSO AREA"
P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient.isin(transform_by)), feature]
P2p1 = df_all.loc[(df_all.Plate==P2) & (df_all.patient.isin(transform_by)), feature]
P1_percentiles = np.percentile(P1p1,percentiles)
P2_percentiles = np.percentile(P2p1,percentiles)
coeff = np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
transformer = np.poly1d(coeff) 
new_df = pd.DataFrame()
new_df2 = pd.DataFrame()

new_df['LYSO AREA'] = df_all.loc[df_all.Plate==P1,feature]
new_df['Plate'] = P1
new_df['patient'] = df_all.loc[df_all.Plate==P1,'patient']

new_df2['LYSO AREA'] = transformer(df_all.loc[df_all.Plate==P2,feature])
new_df2['Plate'] = P2
new_df2['patient'] = df_all.loc[df_all.Plate==P2,'patient']

new_df = new_df.append(new_df2)


P1p1 = df_all.loc[(df_all.Plate==P1) & (df_all.patient.isin(transform_by)), feature]
P3p1 = df_all.loc[(df_all.Plate==P3) & (df_all.patient.isin(transform_by)), feature]
P1_percentiles = np.percentile(P1p1,percentiles)
P3_percentiles = np.percentile(P3p1,percentiles)
coeff = np.polyfit(P3_percentiles,P1_percentiles,poly_deg)
transformer = np.poly1d(coeff) 

new_df3 = pd.DataFrame()

new_df3['LYSO AREA'] = transformer(df_all.loc[df_all.Plate==P3,feature])
new_df3['Plate'] = P3
new_df3['patient'] = df_all.loc[df_all.Plate==P3,'patient']

new_df = new_df.append(new_df3)

new_df.boxplot(column="LYSO AREA", by=['patient','Plate'], showfliers=False, rot=90)


# In[64]:





# # Archive

# In[ ]:


from sklearn import metrics

labels_true = [1,2,3,4,5]
labels_pred = [1,2,3,4,5]

def KL(P,Q):
    """ Epsilon is used here to avoid conditional code for
    checking that neither P nor Q is equal to 0. """
    epsilon = 0.00001

     # You may want to instead make copies to avoid changing the np arrays.
    P = P+epsilon
    Q = Q+epsilon

    divergence = np.sum(P*np.log(P/Q))
    return divergence

# Should be normalized though
values1 = np.asarray([1,2,3,4,5])
values2 = np.asarray([1,2,3,4,5])

KL(values1, values2) 


# In[ ]:


labels_true = np.array([1,2,3,4,10])
labels_true = labels_true/labels_true.sum()
labels_pred = np.array([1,5,3,4,10])
labels_pred = labels_pred/labels_pred.sum()



KL(labels_true, labels_pred) + KL(labels_pred, labels_true) 

