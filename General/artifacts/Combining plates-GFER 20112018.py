
# coding: utf-8

# # Combining plates GFER 20.11.2018
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[320]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[321]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')
# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))


# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns



# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[322]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[323]:


from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

# Do run in the first time: jupyter nbextension enable --py --sys-prefix widgetsnbextension


# In[324]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# In[325]:


from scipy.stats import mannwhitneyu
from scipy import stats


def compare_results(df, base_key, feature_name, df_grpby_key='Prow', hyp_alternative="less"):
    df_grp_row = df.groupby(df_grpby_key)
    base_data = df_grp_row[feature_name].get_group(base_key)

    p_values = {}
    for grp_k in df_grp_row[feature_name].groups.keys():
        if grp_k==base_key:
            continue
        p_data = df_grp_row[feature_name].get_group(grp_k)
        stat, p = mannwhitneyu(base_data, p_data, alternative=hyp_alternative)
        p_values[grp_k] = p
    return p_values


# In[326]:


def pval_corrected(pvals_raw, method=None):
    '''p-values corrected for multiple testing problem
 
    This uses the default p-value correction of the instance stored in
    ``self.multitest_method`` if method is None.
 
    '''
    import statsmodels.stats.multitest as smt
    if method is None:
        method = 'Bonferroni'
    #TODO: breaks with method=None
    return smt.multipletests(pvals_raw, method=method)[1]


# In[327]:


def cum_dist(sample1, sample2, plot=True):
    num_bins = 200
    counts1, bin_edges1 = np.histogram (sample1, bins=num_bins)
    cdf1 = np.cumsum (counts1)
    cdf1 = np.insert(cdf1, 0, 0, axis=0)

    counts2, bin_edges2 = np.histogram (sample2, bins=num_bins)
    cdf2 = np.cumsum (counts2)
    cdf2 = np.insert(cdf2, 0, 0, axis=0)
    
    if plot:
        plt.plot (bin_edges1, cdf1/cdf1[-1])
        plt.plot (bin_edges2, cdf2/cdf2[-1])
        plt.show()
        
    unified = np.append(bin_edges1[1:],bin_edges2[1:])
    unified.sort()

    u1=np.digitize(unified,bin_edges1)
    u1[u1>=num_bins]=num_bins
    v1 = cdf1[u1]/cdf1[-1]

    u2=np.digitize(unified,bin_edges2)
    u2[u2>=num_bins]=num_bins
    v2 = cdf2[u2]/cdf2[-1]
    return ((v2-v1)**2).sum()**0.5         


# In[328]:


# 2,3 HV
# 4,5 HC IK
# 6,7 MV
# 8,9 HC OZ
# 10,11 HC 0730
# 12 AG015

patients = {"AV": {'age': 6*4, "gender": "M", "color": "cornflowerblue"}, 
            "EV": {"age": 5.5*12*4, "gender": "F", "color": "violet"},
            "HC CM": {"age": 6*12*4, "gender": "F", "color": "green"},
            "AG044": {"age": 15, "gender": "F", "color": "limegreen"},
            "AG015": {"age": 3/7, "gender": "M", "color": "cyan"},
            "HV": {"age": 37*12*4, "gender": "F", "color": "darkviolet"},
            "MV": {"age": 37*12*4, "gender": "M", "color": "royalblue"},
            "HC IK": {"age": 32*12*4, "gender": "F", "color": "gray"},
            "HC OZ": {"age": 35*12*4, "gender": "M", "color": "yellow"},
            "HC NA0730":{"age": np.nan, "gender": "M", "color": "coral"}}
patients_df = pd.DataFrame(patients).T
patients_df


# In[329]:


# col_map = {2:"NA0298", 3:"SA0108", 4:"NA0769", 5:"SA0159", 6:"NA0795", 7:"SA0499", 8:"NA0971", 9:"SA0708", 10:"NA1054", 11:"SA1135"}
# col_map
# del col_map


# # Load Data

# In[391]:


data_path1 = "..\\GFER\\Data\\20181120\\more live 1 - adults.XLS"
data_path2 = "..\\GFER\\Data\\20181120\\more live 2 - adults.XLS" # This plate was seeded a day after plate 1


# In[392]:


# read the data from the file
df_live1 = pd.read_excel(data_path1, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df_live1["Plate"] = 1
df_live2 = pd.read_excel(data_path2, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1)
df_live2["Plate"] = 2
df_live = pd.concat([df_live1,df_live2])

print(df_live1.shape)
print(df_live2.shape)
print(df_live.shape)

del df_live1
del df_live2


# In[393]:


# remove redundent column
df_live.drop("Target",axis=1)

# extract the positional data 
loc_df = df_live.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["Prow","Pcol", "Pfield"]
loc_df.Pcol = loc_df.Pcol.astype(np.int16)
df_live = pd.concat([df_live, loc_df], axis=1)
df_live.drop("Section",axis=1, inplace=True)

# replace rows with patients names and join patients data
col_map = {2:"HV", 3:"HV", 4:"HC IK", 5:"HC IK", 6:"MV", 7:"MV", 8:"HC OZ", 9:"HC OZ", 10:"HC 0730", 11:"HC 0730", 12:"AG015"}
mapping_series = pd.Series(list(col_map.values()), index=list(col_map.keys()))
df_live["patient"] = df_live["Pcol"].map(mapping_series)
df_live = df_live.join(patients_df, on='patient')


# In[394]:


df_live.sample(7)


# In[395]:


axes = plt.subplots(1, int(df_live.Plate.max()), figsize=(20,6))

for p,ax in zip(df_live.Plate.unique(), axes[1]):
    cell_in_well = df_live[df_live.Plate==p].groupby(["Prow", "Pcol"])["Pfield"].count()
    cell_in_well = cell_in_well.unstack(level=-1)

    cell_in_well = cell_in_well.rename(col_map, axis=1)

    display(sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f"))


# In[396]:


for axis in ['Prow','Pcol']:
    for plate in df_live.Plate.unique():
        df_by_col = df_live.loc[df_live.Plate==plate, ['Prow','Pcol']].groupby([axis])
        df_by_col_count = df_by_col.count()
        if df_by_col_count.shape[0] == 0:
            continue
        df_by_col_count_q50 = np.percentile(df_by_col_count,50)
        df_by_col_count_q75 = np.percentile(df_by_col_count,75)
        df_by_col_count_q25 = np.percentile(df_by_col_count,25)
        df_by_col_count_irq = df_by_col_count_q75-df_by_col_count_q25
        high_threshold = df_by_col_count_q75 + df_by_col_count_irq * 1.5
        low_threshold = df_by_col_count_q25 - df_by_col_count_irq * 1.5

        ax = df_by_col_count.plot(kind='bar')
        plt.suptitle(f"{axis} - Plate: {plate}")
        plt.hlines([low_threshold, high_threshold], -1, 11, colors='r')
        plt.hlines([df_by_col_count_q50], -1, 11, colors='k')
        plt.hlines([df_by_col_count_q25,df_by_col_count_q75], -1, 11, colors='gray')
        plt.text(0, low_threshold+20, low_threshold, color='r')
        plt.text(0, high_threshold+20, high_threshold, color='r')
        plt.show()
        res = df_by_col_count[(df_by_col_count<low_threshold) | (df_by_col_count>high_threshold)].dropna()
        if res.shape[0]>0:
            df_live[(df_live[axis].isin(res.index)) & (df_live.Plate==plate)] = np.nan    
            print(f"Removing: Plate: {plate} - {axis}: {res.index.tolist()}")
#             display(res)
        plt.show()
df_live.dropna(how='all',inplace=True)


# In[397]:


axes = plt.subplots(1, int(df_live.Plate.max()), figsize=(20,6))

for p,ax in zip(df_live.Plate.unique(), axes[1]):
    cell_in_well = df_live[df_live.Plate==p].groupby(["Prow", "Pcol"])["Pfield"].count()
    cell_in_well = cell_in_well.unstack(level=-1)

    cell_in_well = cell_in_well.rename(col_map, axis=1)

    display(sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f"))


# In[398]:


axis = ['Prow','Pcol']
for plate in df_live.Plate.unique():
    df_by_col = df_live.loc[df_live.Plate==plate, ['Prow','Pcol', 'Plate']].groupby(axis)
    df_by_col_count = df_by_col.count()
    if df_by_col_count.shape[0] == 0:
        continue
    df_by_col_count_q50 = np.percentile(df_by_col_count,50)
    df_by_col_count_q75 = np.percentile(df_by_col_count,75)
    df_by_col_count_q25 = np.percentile(df_by_col_count,25)
    df_by_col_count_irq = df_by_col_count_q75-df_by_col_count_q25
    high_threshold = df_by_col_count_q75 + df_by_col_count_irq * 1.5
    low_threshold = df_by_col_count_q25 - df_by_col_count_irq * 1.5

    ax = df_by_col_count.plot(kind='bar')
    plt.suptitle(f"{axis} - Plate: {plate}")
    plt.hlines([low_threshold, high_threshold], -1, df_by_col_count.shape[0], colors='r')
    plt.hlines([df_by_col_count_q50], -1, df_by_col_count.shape[0], colors='k')
    plt.hlines([df_by_col_count_q25,df_by_col_count_q75], -1, df_by_col_count.shape[0], colors='gray')
    plt.text(0, low_threshold, low_threshold, color='r')
    plt.text(0, high_threshold, high_threshold, color='r')
    plt.show()
    res = df_by_col_count[(df_by_col_count<low_threshold) | (df_by_col_count>high_threshold)].dropna()
    for row, col in res.index.tolist():
        df_live[(df_live['Prow']==row) & (df_live['Pcol']==col) & (df_live.Plate==plate)] = np.nan    
        print(f"Removing: Plate: {plate} - {axis}: {res.index.tolist()}")
#             display(res)
    plt.show()
df_live.dropna(how='all',inplace=True)


# In[399]:


axes = plt.subplots(1, int(df_live.Plate.max()), figsize=(20,6))

for p,ax in zip(df_live.Plate.unique(), axes[1]):
    cell_in_well = df_live[df_live.Plate==p].groupby(["Prow", "Pcol"])["Pfield"].count()
    cell_in_well = cell_in_well.unstack(level=-1)

    cell_in_well = cell_in_well.rename(col_map, axis=1)

    display(sns.heatmap(cell_in_well, linewidths=0.5, ax=ax, annot=True, fmt="0.00f"))


# In[136]:


## What if we ignore the field we are examining?
# plate = 1
# df_by_col = df_live.loc[df_live.Plate==plate, ['Prow','Pcol']].groupby(['Prow'])
# df_by_col_count = df_by_col.count()
# for v in df_by_col_count.index:
#     df_by_col_count_q50 = np.percentile(df_by_col_count[df_by_col_count.index != v],50)
#     df_by_col_count_q75 = np.percentile(df_by_col_count[df_by_col_count.index != v],75)
#     df_by_col_count_q25 = np.percentile(df_by_col_count[df_by_col_count.index != v],25)
#     df_by_col_count_irq = df_by_col_count_q75-df_by_col_count_q25
#     high_threshold = df_by_col_count_q75 + df_by_col_count_irq * 1.5
#     low_threshold = df_by_col_count_q25 - df_by_col_count_irq * 1.5
#     res = df_by_col_count[(df_by_col_count<low_threshold) | (df_by_col_count>high_threshold)].dropna()
#     if res.shape[0]:
#         print (v)
#         print ("="*30)
#         print(low_threshold,high_threshold)
#         display(res)


# In[183]:


# df_live = df_live[df_live["patient"]!= 'AG015']
# df_live = df_live[df_live["Prow"]!='G']


# # Comparing plates

# In[387]:


features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']

features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]


# In[388]:


for col in df_live.columns:
    if col in features_to_ignore:
        continue
    df_live.boxplot(column=col, by=['patient','Plate'], showfliers=False, rot=90)
    plt.show()


# **Questions to look into:**  
# Q1. Are the changes consistent?  
# Q2. HC0730 looks different what would had happen if we would had him as reference?  
# Q3. Can we detect he is different? using anybody?  
# Q4. Does his own columns are "identical" or by detecting a problematic column we can "fix" him?  
#    
#    
# **Answers:**  
# Q1A: The changes looks mostly consistent

# In[310]:


for col in df_live.columns:
    if col in features_to_ignore:
        continue
    fig, ax = plt.subplots(figsize=(15,6)) 
    df_live.boxplot(column=col, by=['patient','Plate', "Pcol"], showfliers=False, rot=90, ax=ax)
    plt.show()


# In[389]:


for col in df_live.columns:
    if col in features_to_ignore:
        continue
    fig, ax = plt.subplots(figsize=(18,8)) 
    df_live.boxplot(column=col, by=['patient','Plate', "Pcol", "Prow"], showfliers=False, rot=90, ax=ax)
    plt.show()


# # Detection of abnormal distributions

# In[312]:


def find_abnomal_wells(df_live, patient, plate, feature, sensitivity = 1.5):
    data_grp = df_live.groupby(['patient','Plate'])
    d = data_grp.get_group((patient, plate))
    data_grp2 = d.groupby(['Prow', 'Pcol'])
    wells = data_grp2.groups
    distances=pd.DataFrame()
    for w1 in wells:
        for w2 in wells:
            if w1 == w2:
                continue
            distances.loc[str(w1),str(w2)] = cum_dist(data_grp2.get_group(w1)[feature],data_grp2.get_group(w2)[feature], plot=False)  

    dist_sum = distances.sum()
    dist_sum50 = np.percentile(dist_sum,50)
    dist_sum75 = np.percentile(dist_sum,75)
    dist_sum25 = np.percentile(dist_sum,25)
    dist_sum_irq = dist_sum75-dist_sum25
    threshold = dist_sum75 + dist_sum_irq * sensitivity
    
    abnormal_wells = dist_sum > threshold
    return abnormal_wells


# In[313]:


features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                      'MITO POS X', 'MITO POS Y',
                      'LYSO POS X', 'LYSO POS Y',
                      'ER POS X', 'ER POS Y',
                      'NUC CG X', 'NUC CG Y',
                      'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                      'patient', 'age','color', 'gender']
features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                          'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]


df_cleaned = df_live.copy()
report = []
for feature in df_live.columns:
    if feature in features_to_ignore:
        continue
    for patient in df_live.patient.unique():
        for plate in df_live.Plate.unique():
            res = find_abnomal_wells(df_live, patient, plate, feature, 2)
            if res.any():
                res_wells = [eval(v) for v in res[res].keys().values]
                report.append((feature, patient, plate, res_wells)) 
                for w in res_wells:
                    row = w[0]
                    col = w[1]
                    df_cleaned.loc[df_live.query(f"patient==\'{patient}\' and Plate=={plate} and Prow==\'{row}\' and Pcol=={col}").index,feature]=np.NaN
report            


# In[314]:


for col in df_cleaned.columns:
    if col in features_to_ignore:
        continue
    fig, ax = plt.subplots(figsize=(18,8)) 
    df_cleaned.boxplot(column=col, by=['patient','Plate', "Pcol", "Prow"], showfliers=False, rot=90, ax=ax)
    plt.show()


# # Alignment of plates 

# In[315]:


# features_to_use = ["TMRE AREA", "TMRE DxA", "MITOTRACKER DxA", "CELL DXA", "CELL AREA"]

import itertools

def transform(df, P1=1, P2=2, transform_by = ['HC IK'], control = ['HC OZ']):
    acceptable_change = 0.15
    testing_range=0.5
    patients_names = df.patient.unique()
    poly_deg = 2

    percentile_range=99
    percentiles = np.linspace(50-percentile_range//2,50+percentile_range//2,(percentile_range+(percentile_range%2==0))*2)

    # transform_by = np.random.choice(patients_names,2, replace=False)

    print(f'{transform_by}')

    features_to_ignore = ['TMRE POS X', 'TMRE POS Y',
                          'MITO POS X', 'MITO POS Y',
                          'LYSO POS X', 'LYSO POS Y',
                          'ER POS X', 'ER POS Y',
                          'NUC CG X', 'NUC CG Y',
                          'Prow', 'Pcol', 'Pfield','Plate', 'passage',
                          'patient', 'age','color', 'gender']
    features_to_ignore = features_to_ignore + ['CELL BRANCH NODES', 'CELL CROSSING POINTS', 'CELL END NODES', 'CELL FIBTMRE LENGTH', 'CELL FRMI', 
                                              'CELL MED DIAMETER', 'NUCLEAR AVG DIAMETTMRE','NUCLEAR WRMI', ]

    for feature in df.columns:
        if feature in features_to_ignore:
            continue
        # It may be that it is fair to move this into the loop and replace transform_by->patient1
        try:
            feature_plate_unification = pd.DataFrame()
            feature_plate_unification_ks = pd.DataFrame()
            feature_plate_unification_cs = pd.DataFrame()
            feature_plate_unification_cs_raw = pd.DataFrame()
            feature_plate_unification_internal = pd.DataFrame()
            
            new_df = pd.DataFrame()
            new_df2 = pd.DataFrame()

            new_df[feature] = df.loc[(df.Plate==P1), feature]
            new_df['Plate'] = P1
            new_df['patient'] = df.loc[df.Plate==P1,'patient']

            new_df2[feature] = df.loc[(df.Plate==P2), feature]
            new_df2['Plate'] = P2
            new_df2['patient'] = df.loc[df.Plate==P2,'patient']

            new_df = new_df.append(new_df2)
            if transform_by:
                P1p1 = df.loc[(df.Plate==P1) & (df.patient.isin(transform_by)), feature].dropna()
                P2p1 = df.loc[(df.Plate==P2) & (df.patient.isin(transform_by)), feature].dropna()
                P1_percentiles = np.percentile(P1p1,percentiles)
                P2_percentiles = np.percentile(P2p1,percentiles)
                coeff = np.polyfit(P2_percentiles,P1_percentiles,poly_deg)
                transformer = np.poly1d(coeff)
                new_df2[feature] = transformer(df.loc[(df.Plate==P2), feature])
                new_df2['Plate'] = P2*100
                new_df2['patient'] = df.loc[df.Plate==P2,'patient']   

                new_df = new_df.append(new_df2)
                
                
                validation_set=set(transform_by+control)

                total=0
                skip_feature = False
                for chk in validation_set:
                    P1chk=new_df.loc[(new_df.patient==chk) & (new_df.Plate==P1),feature].dropna()
                    P2chk=new_df.loc[(new_df.patient==chk) & (new_df.Plate==P2),feature].dropna()
                    P2chk_transformed=new_df.loc[(new_df.patient==chk) & (new_df.Plate==P2*100),feature].dropna()
                    dis=100*(cum_dist(P1chk,P2chk, plot=False)-cum_dist(P1chk,P2chk_transformed, plot=False))
                    print(f"Cummulative dist of {chk} is: {dis}")
                    if dis<=-10:
                        skip_feature=True
                        print(f"skip_feature was set to True")
                    total = total+dis
                if skip_feature or total<len(validation_set)*20:
                    print(f"Skipping tranformation of {feature}!")
                    print(f"total: {total}")
                    continue
                    
            fig, ax = plt.subplots(figsize=(15,6)) 
            new_df.boxplot(column=feature, by=['patient','Plate'], showfliers=False, rot=90, ax=ax)
            plt.show()
        except:
            continue
        for patient1 in patients_names:
            for patient2 in patients_names:    
                P1p1 = df.loc[(df.Plate==P1) & (df.patient==patient1), feature].dropna()
                P1p2 = df.loc[(df.Plate==P1) & (df.patient==patient2), feature].dropna()

                P2p2 = df.loc[(df.Plate==P2) & (df.patient==patient2), feature].dropna()
                feature_plate_unification_cs_raw.loc[patient1,patient2] = cum_dist(P1p1,P2p2, plot=False)       

                if transform_by:
                    P2p2 = transformer(P2p2)
                
                feature_plate_unification_cs.loc[patient1,patient2] = cum_dist(P1p1,P2p2, plot=False)

                P1p1_stats = np.percentile(P1p1, [50-testing_range//2,50,50+testing_range//2])
                P1p2_stats = np.percentile(P1p2, [50-testing_range//2,50,50+testing_range//2])
                P2p2_stats = np.percentile(P2p2, [50-testing_range//2,50,50+testing_range//2])

                P1p1_P1p2_change_percent = (P1p1_stats-P1p2_stats)/np.min([P1p1_stats,P1p2_stats], axis=0)
                P1p1_P2p2_change_percent = (P1p1_stats-P2p2_stats)/np.min([P1p1_stats,P2p2_stats], axis=0)

                P1p1_P1p2_ok = np.all(np.abs(P1p1_P1p2_change_percent)<=acceptable_change)
                P1p1_P2p2_ok = np.all(np.abs(P1p1_P2p2_change_percent)<=acceptable_change)

                feature_plate_unification_internal.loc[patient1,patient2] = P1p1_P1p2_ok*1
                feature_plate_unification.loc[patient1,patient2] = P1p1_P2p2_ok*1

                feature_plate_unification_ks.loc[patient1,patient2] = stats.ks_2samp(P1p1, P2p2).pvalue


        cum_dis_diff = feature_plate_unification_cs_raw-feature_plate_unification_cs
        # between plates patients similarity
    #     patient_similarity = feature_plate_unification.values.diagonal().sum()/feature_plate_unification.values.diagonal().shape[0]
        patient_similarity = (cum_dis_diff.values.diagonal()>=-0.15).sum()/cum_dis_diff.values.diagonal().shape[0]

        # between plates consistency
    #     plates_consistency = (feature_plate_unification==feature_plate_unification_internal).sum().sum()/feature_plate_unification.size


        # patient separation - TODO: fix equation
    #     patients_separation = 1-(feature_plate_unification.sum().sum()-feature_plate_unification.values.diagonal().sum())/(feature_plate_unification.size-feature_plate_unification.values.diagonal().shape[0])


        for patient1 in patients_names:
            feature_plate_unification_ks[patient1] = pval_corrected(feature_plate_unification_ks[patient1].values)

        for patient2 in patients_names:
            feature_plate_unification_ks.loc[:,patient2] = pval_corrected(feature_plate_unification_ks.loc[:,patient2].values)

        if transform_by:
            display(sns.heatmap(cum_dis_diff*100, linewidths=0.5,  annot=True, fmt="0.000f"))
            plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\n")
        else:
            display(sns.heatmap(feature_plate_unification_cs_raw*100, linewidths=0.5,  annot=True, fmt="0.000f"))

    #     display(sns.heatmap(feature_plate_unification*100, linewidths=0.5,  annot=True, fmt="0.000f"))
    #     plt.suptitle(f"{feature}\n{'-'*len(feature)*3}\nPatient similarity between plates: {patient_similarity*100:0.2f}%\nPlates consistency: {plates_consistency*100:0.2f}%\nPatients separation: {patients_separation*100:0.2f}%")
        plt.show()


# In[316]:


# transform(df_live, P1=1, P2=2, transform_by = [], control = [])


# In[317]:


# What is the 350.98 and why is it not in the


# In[318]:


transform(df_live, P1=1, P2=2, transform_by = ['HC IK', 'MV'], control = ['HC OZ'])


# In[319]:


transform(df_cleaned, P1=1, P2=2, transform_by = ['HC IK', 'MV'], control = ['HC OZ'])


# Cleaning abnormal well does not seem to contribute significantly and thus should not be done.
