#!/usr/bin/env python
# coding: utf-8

# # Comparing results CellDoctor-Developer
# _Maydan Wienreb_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[2]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[3]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[4]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # Loading CellDoctor the data

# In[5]:


data_path = "../GFER/Data/Lyco/Lyco plate per person 27.03.2019/Alon 24h - agg_results.csv"
df = pd.read_csv(data_path, index_col=0)
df = df.drop(labels='index', axis=1)
df = df[df.nuc_outlier==False]
df = df[df.cyto_outlier==False]
df = df[df.Cyto_border_case==False]
df = df.drop(labels=['nuc_outlier', 'cyto_outlier', 'Cyto_border_case'], axis=1)
df.columns = df.columns.str.replace("GFER", "Cox17") # This is due to a naming mistake at the data creation


# In[6]:


df.sample(7)


# In[7]:


# Adding Compunds and concentration 
df["Compound"] = np.nan
df.loc[df.column.isin([2, 3]), "Compound"] = "Control"
df.loc[df.column.isin([4, 5]), "Compound"] = "DMSO"
df.loc[df.column.isin([6, 7]), "Compound"] = "Lyco Low"
df.loc[df.column.isin([8, 9]), "Compound"] = "Lyco Medium"
df.loc[df.column.isin([10, 11]), "Compound"] = "Lyco High"
display('ok'if df["Compound"].isna().any()==False else ':-(')


# In[8]:


compound_plate = df.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# # Loading Developer data

# In[15]:


data_path = "../GFER/Data/Lyco/Lyco plate per person 27.03.2019/AV 24h - Standard lab acquisition protocol GFER IF_AVlyco24hcox17andmito_1.2019.03.27.11.22.52.xlsx"


# In[27]:


df_dev = pd.read_excel(data_path, sheet_name='Nuc_cell_TMRE_MITOTRACKER', header=1, skiprows=0)
# remove redundent column
df_dev.drop("Target",axis=1, inplace=True)
# extract the positional data 
loc_df = df_dev.Section.str.extract("(.*) - (.*) \(fld (.*)\).*",expand=True)
loc_df.columns=["row","column", "field"]
loc_df.column = loc_df['column'].astype(np.int16)
df_dev = pd.concat([df_dev, loc_df], axis=1)
df_dev.drop("Section",axis=1, inplace=True)


# In[28]:


df_dev.sample(7)


# In[32]:


# Adding Compunds and concentration 
df_dev["Compound"] = np.nan
df_dev.loc[df_dev.column.isin([2, 3]), "Compound"] = "Control"
df_dev.loc[df_dev.column.isin([4, 5]), "Compound"] = "DMSO"
df_dev.loc[df_dev.column.isin([6, 7]), "Compound"] = "Lyco Low"
df_dev.loc[df_dev.column.isin([8, 9]), "Compound"] = "Lyco Medium"
df_dev.loc[df_dev.column.isin([10, 11]), "Compound"] = "Lyco High"
display('ok'if df_dev["Compound"].isna().any()==False else ':-(')


# In[33]:


compound_plate = df_dev.groupby(['column', 'row']).Compound.first().unstack(level=-1).T
compound_plate


# # Number of cells 

# In[49]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2)
fig.set_size_inches(18, 6)

cell_in_well = df.groupby(["row", "column"])["Compound"].count().unstack(level=-1)
ax[0] = sns.heatmap(cell_in_well, linewidths=0.5, annot=True, fmt="0.00f", ax=ax[0])
ax[0].set_title("CellDoctor")

cell_in_well_dev = df_dev.groupby(["row", "column"])["Compound"].count().unstack(level=-1)
ax[1] = sns.heatmap(cell_in_well_dev, linewidths=0.5, annot=True, fmt="0.00f", ax=ax[1])
ax[1].set_title("Developer")

plt.tight_layout()


# From This comparison by developer in a random field at G10 there should not be any cells. Let's check it out.

# In[174]:


img = plt.imread('./Data/AV Lyco 24h G-10 fld 08 (comparing results).jpg')
plt.imshow(img)


# Let look how a random field at G9 looks like

# In[152]:


img = plt.imread('./Data/AV Lyco 24h G-09 fld18 (comparing results).jpg')
plt.imshow(img)


# In[175]:


img.shape


# In[207]:


import json
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.colors import to_rgba


def load_img_and_segmentation(img_path, seg_path):
    img = plt.imread(img_path)

    with open(seg_path, 'r') as f:
        segmentation_data = json.load(f)
        

    fig, ax = plt.subplots(1,1)

    ax.imshow(img)
    patches = []

    for shape in segmentation_data['shapes']:
        points = np.array(shape['points'])
        color = to_rgba(np.array(shape['fill_color'])/255)
        points[:,0]=(points[:,0]*img.shape[0]/2048)
        points[:,1]=(points[:,1]*img.shape[1]/2048)
        points[:,1] = points[:,1]-10  # am not sure why is is needed probably due to the printscreen changes in resultion and image size  
        ax.add_patch(Polygon(points, True, linewidth=2 ,edgecolor=color,facecolor=color,alpha=0.5))

    plt.show()        


# In[208]:


load_img_and_segmentation('./Data/AV Lyco 24h G-10 fld 08 (comparing results).jpg', './Data/G - 10(fld 08 wv Cy3 - Cy3).json')


# In[209]:


load_img_and_segmentation('./Data/AV Lyco 24h G-09 fld18 (comparing results).jpg', './Data/G - 09(fld 18 wv Cy3 - Cy3).json')


# In[ ]:





# In[155]:


order = ['Control', 'DMSO', 
         'Lyco Low', 'Lyco Medium', 'Lyco High'
        ]


# In[158]:


g = df.groupby(['Compound'])["row"].count().reset_index()
sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=True)
fig.set_size_inches(19, 8)

sns.barplot(x="Compound", y="row", hue="Compound", data=g, palette="Set3" ,ax=ax[0], ci=None, hue_order=order, order=order);
ax[0].set_title("Cell Doctor - Cells Count")
ax[0].legend(loc = 1)

g_dev = df_dev.groupby(['Compound'])["row"].count().reset_index()
sns.barplot(x="Compound", y="row", hue="Compound", data=g_dev, palette="Set3" ,ax=ax[1], ci=None, hue_order=order, order=order);
ax[1].set_title("Developer - Cells Count")
ax[1].legend(loc = 1)

plt.tight_layout()


# # Analysis of results

# In[164]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cyto_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax[0], hue_order=order, order=order)
ax[0].set_title('CellDoctor - Cyto size')

sns.boxplot(x="Compound", y="CELL AREA", hue="Compound", data=df_dev, palette="Set3", showfliers=False, whis = [10,90], ax=ax[1], hue_order=order, order=order)
ax[1].set_title('Developer - CELL AREA')

plt.tight_layout()


# In[163]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_size", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax[0], hue_order=order, order=order)
ax[0].set_title('CellDoctor - Cox17 size')

sns.boxplot(x="Compound", y="TMRE AREA", hue="Compound", data=df_dev, palette="Set3", showfliers=False, whis = [10,90], ax=ax[1], hue_order=order, order=order)
ax[1].set_title('Developer - TMRE AREA')

plt.tight_layout()


# In[165]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="Cox17_avg", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax[0], hue_order=order, order=order)
ax[0].set_title('CellDoctor - Cox17 avg')

sns.boxplot(x="Compound", y="TMRE INTENSITY", hue="Compound", data=df_dev, palette="Set3", showfliers=False, whis = [10,90], ax=ax[1], hue_order=order, order=order)
ax[1].set_title('Developer - TMRE INTENSITY')

plt.tight_layout()


# In[168]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="MitoTracker_count", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax[0], hue_order=order, order=order)
ax[0].set_title('CellDoctor - MitoTracker_count')

sns.boxplot(x="Compound", y="MITOTRACKER COUNT", hue="Compound", data=df_dev, palette="Set3", showfliers=False, whis = [10,90], ax=ax[1], hue_order=order, order=order)
ax[1].set_title('Developer - MITOTRACKER COUNT')

plt.tight_layout()


# In[169]:


sns.set_style('ticks')
fig, ax = plt.subplots(1,2, sharey=False)
fig.set_size_inches(19, 8)
sns.boxplot(x="Compound", y="MitoTracker_area_mean", hue="Compound", data=df, palette="Set3", showfliers=False, whis = [10,90], ax=ax[0], hue_order=order, order=order)
ax[0].set_title('CellDoctor - MitoTracker_area_mean')

sns.boxplot(x="Compound", y="MITOTRECKER AREA", hue="Compound", data=df_dev, palette="Set3", showfliers=False, whis = [10,90], ax=ax[1], hue_order=order, order=order)
ax[1].set_title('Developer - MITOTRECKER AREA')

plt.tight_layout()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




