
# coding: utf-8

# # Huntington analysis
# _Saja_
# <img src="https://raw.githubusercontent.com/maydanw/InEasyReach/master/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# In[1]:


get_ipython().run_cell_magic('javascript', '', "$.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')")


# <h1 id="tocheading">Table of Contents</h1>
# <div id="toc"></div>

# # Imports

# In[15]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

from typing import Dict, Tuple, List

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import graphics
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
import seaborn as sns
sns.set_style("whitegrid")

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[16]:


import types
import sys
print ("Basic packages versions:")
print ("   Python version: %s" % (sys.version))
print ("   Packages:")


try:  
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))
except: 
    g=globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print ("   %s: %s" % (mod.__name__, mod.__version__))


# # Loading Data

# In[17]:


data_path = "C:\\BioData\\HD\\Standard lab acquisition protocol 5_D.CAL.TM.MITO MIX1 24H+SER_1\\results\\agg_results.csv"


# In[21]:


df = pd.read_csv(data_path, index_col=0, low_memory=False)
df.sample(7)


# In[22]:


df.columns.tolist()


# In[23]:


print(f"Dataset size: {df.shape[0]} rows, {df.shape[1]} columns")
df.describe()


# ## How the experiment looks like
# 
# **Saja: to change the subjects**
# 
# |Number|Name|Gender|Age|
# |---|---|---|---|
# |3|AV|M|0|
# |4|MV|M|37|
# |5|EV|F|5|
# |6|HV|F|37|
# |7|Healthy IK|F|32 |
# |8|Healthy OZ|M|35| 
# |9|Healthy CM|F|6| 

# # Pre-Processing
# ## Remove outliers
# 

# Removing outliers which were detected as nucleus 

# In[24]:


print(f"Starting with dataset size: {df.shape[0]:,} rows, {df.shape[1]} columns")
if 'outlier' in df.columns:
    print(f"Removing {df[df.outlier].shape[0]:,} rows and 1 column")
    df.drop(df[df.outlier].index, axis=0, inplace=True, errors='ignore')
    df.drop('outlier', axis=1, inplace=True, errors='ignore')
    print(f"Dataset size: {df.shape[0]:,} rows, {df.shape[1]} columns")


# Removing border cases

# In[25]:


# if 'Nucli_border_case' in df.columns:
#     print(f"Removing {df[df.Nucli_border_case].shape[0]:,} rows and 1 column")
#     df.drop(df[df.Nucli_border_case].index, axis=0, inplace=True, errors='ignore')
#     df.drop('Nucli_border_case', axis=1, inplace=True, errors='ignore')
#     print(f"Dataset size: {df.shape[0]:,} rows, {df.shape[1]} columns")


# In[26]:


if 'Cyto_border_case' in df.columns:
    print(f"Removing {df[df.Cyto_border_case].shape[0]} rows and 1 column")
    df.drop(df[df.Cyto_border_case].index, axis=0, inplace=True, errors='ignore')
    df.drop('Cyto_border_case', axis=1, inplace=True, errors='ignore')
    print(f"Dataset size: {df.shape[0]} rows, {df.shape[1]} columns")


# In[28]:


print(f"Dataset size: {df.shape[0]} rows, {df.shape[1]} columns")
df.describe()


# ### TODO: add proper outliers detection

# ## Removing redundent features

# In[11]:


df.drop(df.columns[df.columns.str.contains('nuc_in_cyto')],axis=1, inplace=True)
df.drop(df.columns[df.columns.str.contains('nucli_label')],axis=1, inplace=True)
df.drop(df.columns[df.columns.str.contains('cyto_label')],axis=1, inplace=True)


# ## Set types of fields

# In[12]:


df["column"] = df["column"].astype('category') 
df["field"] = df["field"].astype('category')
df["row"] = df["row"].astype('category')


# In[13]:


df.loc[:,df.dtypes!=np.float64].dtypes


# This list makes sense.

# ## Data Imputation

# In[14]:


print(f"There are {df.isna().any(axis=1).shape[0]:,} rows with missing data")


# Missing data can be a result of multiple cases:
# 1. Cyto segmentation is missing/wrong
# 1. No mitochondria was found in the cytoplasm
# 1. There is only one mitochondria in the cytoplasm
# 1. There are only two mitochondria in the cytoplasm  
# 1. Some parameter could not be calculated (division by zero, ...)
# 
# We will need to handle each of these independently

# ### Cyto segmentation is missing/wrong
# These row should be removed

# In[15]:


cyto_columns = df.columns[df.columns.str.startswith('Cyto_')]
display(df[df[cyto_columns].isna().all(axis=1)])
print(f"{df[df[cyto_columns].isna().all(axis=1)].shape[0]:,} rows were found and removed")
df.drop(df[df[cyto_columns].isna().all(axis=1)].index, axis=0, inplace=True, errors='ignore')


# ### No mitochondria was found in the cytoplasm
# Let's use MitoTracker_area as an indictor

# In[16]:


display(df[df.MitoTracker_area_mean.isna()])
print(f"{df[df.MitoTracker_area_mean.isna()].shape[0]:,} were found")


# In[17]:


display(df[df.TMRE_area_mean.isna()])
print(f"{df[df.TMRE_area_mean.isna()].shape[0]:,} were found")


# In[18]:


df[df.isna().any(axis=1)]


# In[19]:


numeric_cols=df.loc[:,df.dtypes==np.float64].columns
# df[numeric_cols] = df[numeric_cols].fillna(value=-1.0,axis=1)
df[numeric_cols] = df[numeric_cols].fillna(value=-1.0,axis=1)


# ## Calculatate decay of rows
# Let's look at the intensity q10, median, q90 by row and see if there is a decay. This need to be done per channel.

# In[20]:


df.MitoTracker_avg_mean


# In[21]:


df[['Cyto_avg', 'Nucli_avg', 'TMRE_avg_mean' ,'MitoTracker_avg_mean','row']].groupby(by='row').agg(['mean','std'])


# It looks like:
# * Nucli: Does have a decline both in avg as well as in std
# Let's verify these results for Nucli across few percentile measurments

# In[22]:


df[['Nucli_q10', 'Nucli_q50', 'Nucli_q90', 'row']].groupby(by='row').agg(['mean','std'])


# <div class="alert alert-warning" role="alert">
# Add proper statistical tests and make corrections. <br>
# Adjustments need to be fitted with LR
# </div>

# In[23]:


df['TMRE_MitoTracker_avg_ratio'] = df['TMRE_avg_mean']/df['MitoTracker_avg_mean']
df['TMRE_MitoTracker_area_ratio'] = df.TMRE_area_mean/df.MitoTracker_area_mean


df[['TMRE_MitoTracker_avg_ratio', 'TMRE_MitoTracker_area_ratio']]


# In[24]:


df.drop(df.columns[df.columns.str.contains('MitoTracker_cx')], axis=1, inplace=True, errors='ignore')
df.drop(df.columns[df.columns.str.contains('MitoTracker_cy')], axis=1, inplace=True, errors='ignore')

df.drop(df.columns[df.columns.str.contains('TMRE_cx')], axis=1, inplace=True, errors='ignore')
df.drop(df.columns[df.columns.str.contains('TMRE_cy')], axis=1, inplace=True, errors='ignore')


# 
# ## Features starndatization
# ## Convert Y to category
# 
# ## Grouping
# 

# In[25]:


columns_map = {3: "AV", 4: "MV", 5: "EV", 6: "HV", 7: "h IK", 8: "h OZ", 9: "h CM"}
df['column'] = df['column'].map(columns_map)


# In[26]:


df[['Cyto_avg', 'Nucli_avg','TMRE_avg_mean' ,'MitoTracker_avg_mean','Cyto_area','TMRE_MitoTracker_avg_ratio','TMRE_MitoTracker_area_ratio','column']].groupby(by='column').agg(['mean','std','count']).T


# ## PCA

# In[90]:


import sklearn
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
colors={"AV":'blue', 
       "EV":'cyan', 
       "HV": 'teal',
       "MV": "cornflowerblue",
       "h CM": "gold",
       "h IK": "orange",
       "h OZ": 'red'}


PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=2))])
# X = df.groupby(by=['column','row']).agg(['mean']).dropna(axis=1)
X = df.drop(['index'], axis=1).groupby(by=['column','row']).agg(['mean']).fillna(-1)


#_____________________________________________
# Removing Nucli and Cyto features for testing
#_____________________________________________
# X.drop('h2', inplace=True)
# X.drop(df.columns[df.columns.str.contains('Cyto')],axis=1, inplace=True)
# X.drop(df.columns[df.columns.str.contains('Nucli')],axis=1, inplace=True)

#_____________________________________________

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X)
fig, ax = plt.subplots()

ind = 0
for g in df.column.unique():
    inc = X.loc[g, :].shape[0]
    ax.scatter(X_pc[ind:ind+inc, 0], X_pc[ind:ind+inc, 1], c=colors[X.index[ind][0]], alpha=0.9, label=X.index[ind][0])
    ind = ind + inc
    


ax.legend(loc='upper right')
ax.set_title("PCA")
ax.figure.set_size_inches((10,8))
plt.show()


# In[77]:


import sklearn
from mpl_toolkits.mplot3d import Axes3D

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline

PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=3))])
X = df.groupby(by=['column','row']).agg(['mean']).fillna(0)

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X)

fig = plt.figure(1, figsize=(10, 10))
plt.clf()
ax = Axes3D(fig, elev=30, azim=150)

ind = 0
for g in df.column.unique():
    inc = X.loc[g, :].shape[0]
    ax.scatter(X_pc[ind:ind+inc, 0], X_pc[ind:ind+inc, 1], c=colors[X.index[ind][0]], alpha=0.9, label=X.index[ind][0], s=80)
    ind = ind + inc
    
ax.legend(loc='upper right')
ax.set_title("PCA 3D")
ax.figure.set_size_inches((10,10))
plt.show()


# In[102]:


import sklearn
from mpl_toolkits.mplot3d import Axes3D

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
colors={"AV":'blue', 
       "EV":'cyan', 
       "HV": 'teal',
       "MV": "cornflowerblue",
       "h CM": "gold",
       "h IK": "orange",
       "h OZ": 'red'}


PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=2))])
# X = df.groupby(by=['column','row']).agg(['mean']).dropna(axis=1)
# X = df.groupby(by=['column','row']).agg(['mean']).fillna(-1)
X = df.drop(['index'], axis=1)




#_____________________________________________
# Removing Nucli and Cyto features for testing
#_____________________________________________
# X.drop('h2', inplace=True)
# X.drop(df.columns[df.columns.str.contains('Cyto')],axis=1, inplace=True)
# X.drop(df.columns[df.columns.str.contains('Nucli')],axis=1, inplace=True)

#_____________________________________________

for f in X.columns[X.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X.drop(['column'], axis=1))
fig, ax = plt.subplots()

for g in df.column.unique():
    ax.scatter(X_pc[X.column==g, 0], X_pc[X.column==g, 1], c=colors[g], alpha=0.75, label=g)
    


ax.legend(loc='upper right')
ax.set_title("PCA")
ax.figure.set_size_inches((10,8))
plt.show()


# In[103]:


import sklearn
from mpl_toolkits.mplot3d import Axes3D

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline

PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=3))])
X = df.drop(['index'], axis=1)

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X.drop(['column'], axis=1))

fig = plt.figure(1, figsize=(10, 10))
plt.clf()
ax = Axes3D(fig, elev=30, azim=150)

ind = 0
for g in df.column.unique():
    ax.scatter(X_pc[X.column==g, 0], X_pc[X.column==g, 1], c=colors[g], alpha=0.75, label=g)
    
ax.legend(loc='upper right')
ax.set_title("PCA 3D")
ax.figure.set_size_inches((10,10))
plt.show()


# Lrt's try to take HV out of the poplulation

# In[30]:


X = df.groupby(by=['column','row']).agg(['mean']).fillna(-1)


#_____________________________________________
# Removing HV
#_____________________________________________
X.drop('HV', level='column', inplace=True)
#_____________________________________________

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X)
fig, ax = plt.subplots()

ind = 0
for g in X.index.get_level_values(0).unique():
    inc = X.loc[g, :].shape[0]
    ax.scatter(X_pc[ind:ind+inc, 0], X_pc[ind:ind+inc, 1], c=colors[X.index[ind][0]], alpha=0.9, label=X.index[ind][0])
    ind = ind + inc
    


ax.legend(loc='upper right')
ax.set_title("PCA")
ax.figure.set_size_inches((10,8))
plt.show()


# Let's try to use Random Foreset to find features importance and do a PCA on selected features

# In[31]:


X = df.copy().dropna()
y = X.column
y = pd.get_dummies(y)
for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')
X.drop('column', axis=1, inplace=True, errors='ignore')
X.drop('index', axis=1, inplace=True, errors='ignore')
X.drop('nucli_label', axis=1, inplace=True, errors='ignore')
X.drop('cyto_label', axis=1, inplace=True, errors='ignore')


# In[32]:


from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=500, max_depth = 4)
clf.fit(X, y)


# In[33]:


importances = clf.feature_importances_
std = np.std([tree.feature_importances_ for tree in clf.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(100):
    print("%d. feature %s (%f)" % (f + 1, X.columns[indices[f]], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(X.shape[1]), importances[indices],
       color="r", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()


# In[34]:


PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=2))])
X = df.groupby(by=['column','row']).agg(['mean']).fillna(0)
X=X[X.columns[indices[:25]]]

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')


PCA_pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=2))])

for f in df.columns[df.dtypes=='category']:
    X.drop(f, axis=1, inplace=True, errors='ignore')

X_pc = PCA_pipeline.fit_transform(X)
fig, ax = plt.subplots()

ind = 0
for g in df.column.unique():
    inc = X.loc[g, :].shape[0]
    ax.scatter(X_pc[ind:ind+inc, 0], X_pc[ind:ind+inc, 1], c=colors[X.index[ind][0]], alpha=0.9, label=X.index[ind][0])
    ind = ind + inc
    
ax.legend(loc='upper right')
ax.set_title("PCA")
ax.figure.set_size_inches((12,8))
plt.show()    
    

